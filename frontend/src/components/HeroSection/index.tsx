import {useState} from 'react'
import Video from '../../videos/video.mp4'
import { HeroContainer, HeroBg, VideoBg, HeroContent, HeroH1, HeroP} from './HeroElements'
// import { useHistory } from "react-router-dom";


const HeroSection = () => {
    const [hover, setHover] = useState(false)

    const onHover = () => {
        setHover(!hover)
    }
    window.history.pushState(null, "", window.location.href);
    window.onpopstate = function () {
        window.history.pushState(null, "", window.location.href);
    };


  return (
    <HeroContainer>
        <HeroBg>
            <VideoBg autoPlay loop muted >
                <source src={Video} type="video/mp4" />
            </VideoBg>
        </HeroBg>
        <HeroContent>
            <HeroH1>Online-Broker einfach gemacht</HeroH1>
            <HeroP>
                Registriere dich noch heute und erhalte 10.000€ für deinen ersten Aktienkauf.
            </HeroP>
        </HeroContent>
    </HeroContainer>
  )
}

export default HeroSection