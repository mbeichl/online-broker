import { Router } from "express";
import accController from "./controller";

const accRouter = Router();

accRouter.post('/cheatcards',accController.getCheatcards)

export default accRouter;
