import React, { createContext, useState } from 'react'

export interface IUser {
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    password: string;
    accountvalue: number;
    portfolio: number;
    cheatcards: number
}

export type UserContextType = {
  user: IUser;
  saveUser: (u: IUser) => void;
}


interface Props {
  children: React.ReactNode;
}


export const UserContext = createContext<UserContextType | null>(null);

export type UserProps = {
  user: IUser;
}

const UserProvider: React.FC<Props> = ( {children} ) => {
    const [user, setUser] = useState<IUser>({
        id: 0,
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        password: '',
        accountvalue: 100000,
        portfolio: 0,
        cheatcards: 3
      });

        const saveUser = (iuser:IUser) => {
            const newUser: IUser = iuser;
            setUser(newUser);
        }

        window.onbeforeunload = (event) => {
          const e = event || window.event;
          // Cancel the event
          e.preventDefault();
          if (e) {
            e.returnValue = ''; // Legacy method for cross browser support
          }
          return ''; // Legacy method for cross browser support
        };

  return (
    <>
    <UserContext.Provider value={{ user, saveUser}}>
      {children}
    </UserContext.Provider>
    </>
  )
}

export default UserProvider