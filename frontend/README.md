# Setup
- `npx create-react-app frontend --template=typescript`

## Packages installieren
- `npm install react react-icons styled-components react-scroll react-router-dom chart.js react-chartjs-2 react-toastify`
- `npm install --save @types/react @types/react-router-dom @types/styled-components @types/react-scroll`

## Frontend starten
- `npm start`


## ToDos Frontend:

- Footer
  - Seiten erstellen
  - Seiten verlinken
  - Inhalte erstellen
- Navbar
  - wenn nicht gescrollt Navbar transparent auf Heropage
- Sidebar
  - bei verkleinertem Bildschirm angezeigt
  - bei kleinem Bildschirm öffnen & schließen
  - bei kleinem Bildschirm Links benutzen
- HeroSection 
  - Register Page verlinken
- SignIn
  - Form erstellen
  - Backend verknüpfen
- Register
  - Page erstellen
  - Form erstellen
  - Backend verknüpfen
- NavbarNachLogin
- SidebarNachLogin
- Stocks
- Portfolio
- Watchlist