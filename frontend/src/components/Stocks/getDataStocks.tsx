import React, { useEffect, useState } from "react";
import { StockWithValue } from "./Stock";
import { Line } from "react-chartjs-2";
import "reactjs-popup/dist/index.css";
import { IUser, UserContext, UserContextType } from "../../UserProvider";
import "reactjs-popup/dist/index.css";
import { Value } from "../Portfolio";
import { ToastContainer, toast } from "react-toastify";
import { AiFillStar, AiOutlineStar } from "react-icons/ai";
import Popup from "../Portfolio/Popup";
import { PopupContainer } from "../Portfolio/PortfolioElements";

type watchlistContent = {
  isin: string;
};

const GetDataStocks = () => {
  const { user, saveUser } = React.useContext(UserContext) as UserContextType;
  let userdata: IUser = user;

  const [stocks, setStocks] = useState<StockWithValue[]>([]);
  const [value, setValues] = useState<Value[]>([]);
  const [refetch, setRefetch] = useState(0);
  const [watchlist, setWatchlist] = useState<watchlistContent[]>([]);

  const [buttonPopup, setButtonPopup] = useState(false);

  //die Quantitys die in input eingetragen werden , werden als Mamp Key: ISIN, Value: quantity gespeichert und angezeigt
  var quantityMap: Map<string, number> = new Map<string, number>();

  function setQuantity(isin: string, quantity: number) {
    if (quantity >= 0) {
      quantityMap.set(isin, quantity);
    }
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/getAllStocks`, {
      headers: {
        "Content-Type": "application/json",
        //authorization: `Bearer ${localStorage.getItem('tocken')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => setStocks(data));

    setValues([]);
    fetch(`${process.env.REACT_APP_BACKEND_URL}/purchases/stocks`, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => setValues(data));

    fetch(
      `${process.env.REACT_APP_BACKEND_URL}/watchlist/getWatchlist/${user.id}`,
      {
        headers: { "Content-Type": "application/json" },
      }
    )
      .then((response) => response.json())
      .then((data) => setWatchlist(data));
  }, [refetch]);

  function getWatchlist(isin: string) {
    const arr: watchlistContent[] = watchlist;
    let check: boolean = false;
    arr.map((element) => {
      console.log(element);
      if (element.isin == isin) {
        check = true;
      }
    });
    if (check) {
      return <AiFillStar size={25} />;
    }
    return <AiOutlineStar size={25} />;
  }

  function updateWatchlist(isin: string) {
    const newData = {
      userid: user.id,
      isin: isin,
    };

    let check: boolean = false;
    watchlist.forEach((element) => {
      if (element.isin == isin) {
        check = true;
      }
    });

    if (check) {
      //bereits vorhanden -> aus DB löschen

      fetch(
        `${process.env.REACT_APP_BACKEND_URL}/watchlist/deleteWatchlistItem`,
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(newData),
        }
      )
        .then((response) => response.text())
        .then((text) => {
          if (text.toString() === "OK") {
            setRefetch(Math.random());
          } else {
            toast(
              `❌ Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut! `
            );
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    } else {
      //noch nicht vorhanden -> Update in DB
      fetch(`${process.env.REACT_APP_BACKEND_URL}/watchlist/updateWatchlist`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(newData),
      })
        .then((response) => response.text())
        .then((text) => {
          if (text === "OK") {
            setRefetch(Math.random());
          } else {
            toast(
              `❌ Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut! `
            );
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });
    }
  }

  async function PurchaseStock(stockWithValue: StockWithValue) {
    console.log("einfsach cliiicken");
    console.log(JSON.stringify(stockWithValue));
    var success: string = "";
    var body: string = `{
        "isin": "${stockWithValue.isin}",
        "name": "${stockWithValue.name}",
        "type": "buy",
        "stockprice": ${stockWithValue.stockprice},          
        "currentdate": "${stockWithValue.currentdate}",          
        "growthrate": "${stockWithValue.growthrate}",
        "quantity": ${quantityMap.get(stockWithValue.isin)},
        "userid": ${userdata.id}          
      }`;

    const stueck = parseFloat(
      (
        document.getElementById(
          "quantity" + stockWithValue.isin
        ) as HTMLInputElement
      ).value
    );
    const stuecksss = quantityMap.get(stockWithValue.isin);

    let kursFloat = parseFloat(stockWithValue.stockprice.toString());
    const pv = parseFloat(user.portfolio.toString());
    let portfoliovalue;

    if (pv + kursFloat * stueck < 0) {
      portfoliovalue = 0;
    } else {
      portfoliovalue = parseFloat((pv + kursFloat * stueck).toFixed(2));
    }
    let accval = parseFloat(user.accountvalue.toString()) - kursFloat * stueck;
    let accvalue = parseFloat(accval.toFixed(2));

    if (
      parseFloat((kursFloat * stueck).toFixed(2)) >
      parseFloat(user.accountvalue.toString())
    ) {
      toast(`❌ Nicht genügend Geld! `);
    } else {
      const newData = {
        id: user.id,
        accountvalue: accvalue,
        portfolio: portfoliovalue,
      };

      const newUser: IUser = {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        userName: user.lastName,
        email: user.email,
        password: user.password,
        accountvalue: accvalue,
        portfolio: portfoliovalue,
        cheatcards: 3,
      };

      fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/buy`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          //authorization: `Bearer ${localStorage.getItem('tocken')}`,
        },
        body: body,
      })
        .then((response) => response.json())
        .then((data) => {
          toast(data);
        });

      //Update User in DB
      fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/updateUser`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify(newData),
      })
        .then((response) => {
          if (response.ok) {
            console.log("Success");
          } else {
            console.log("Error");
          }
        })
        .catch((error) => {
          console.error("Error:", error);
        });

      saveUser(newUser);
      setRefetch(Math.random());
      (
        document.getElementById(
          "quantity" + stockWithValue.isin
        ) as HTMLInputElement
      ).value = "";
    }
  }

  function getGrowthRateColor(growthrate: string): string {
    var growthrateColor: string;
    console.log("des coloeurs")
    if (growthrate.at(0) == "-") {
      return "red";
    }

    if (growthrate.at(0) == "+") {
      return "green";
    }
    return "grey";
  }

  function getGrowthRate(growthrate: string): string {
    console.log("getGrowthRate")
    
    if (growthrate.at(0) == "-") {
      return growthrate + " " + "%       " + "\u2b07";
    }

    if (growthrate.at(0) == "+") {
      return growthrate + " " + "%       " + "\u2B06";
    }
    return growthrate + " %";
  }

  function getLineGraph(isin: String) {
    const arr: Value[] = [];
    let dateString: string;

    value.map((i) => {
      if (i.sv_isin === isin) {
        dateString = i.currentdate.substring(0, 10);
        i.currentdate = dateString;
        arr.push(i);
      }
    });

    //arr.sort(compare)

    const dataSet = {
      labels: arr.map((item) => item.currentdate),
      datasets: [
        {
          data: arr.map((item) => item.stockprice),
          borderColor: "rgb(255, 99, 132)",
          backgroundColor: "rgba(255, 99, 132, 0.5)",
        },
      ],
    };

    const option = {
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        scales: {
          scales: {
            x: [
              {
                ticks: {
                    display: false
                }
            }],
              yAxes: [{
                display: false
              }]
            }
          }
        }
      };
      return <Line options={option} data={dataSet}/>; //<Line data={}/>
  
    }

  if (!stocks.length) {
    return (
      <>
        <div className="wrapper">
          <div className="header">
            <h2>Stocks</h2>
            <p
              style={{ textAlign: "center", marginTop: "10rem", color: "red" }}
            >
              Keine Aktien gefunden, bitte versuchen Sie es erneut!
            </p>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="wrapper">
        <div className="header">
          <h2>Börse</h2>
          {/* <p>Portfoliowert: {user.portfolio} €</p> */}
        </div>
        <div className="body">
          {/* <div className='wrapper' >
          <div className='header'>
          </div>
          <div className='body'> */}

          <div className="main-container">
            <div className="table-container">
              {" "}
              {/*style={{"width" : "95%"}} */}
              <div className="table-row headinger">
                <div className="row-itemsmall">
                  <AiFillStar size={25} />
                </div>
                <div className="row-item2">Name</div>
                <div className="row-item">ISIN</div>
                <div className="row-item">Anlagetyp</div>
                <div className="row-itemsmall">Kurs</div>
                <div className="row-item4">Graph</div>
                <div className="row-item">Trend</div>
                <div className="row-item">Kaufen</div>
                <div className="row-item"></div>

            {/* <div className="row-item"><img width="30" height="30" src="https://png2.cleanpng.com/sh/b41f40dac46116ae1a756a5d8e14b33d/L0KzQYi4UcI4N5c5UZGAYUHmR7O6hMU1bGY1TZC6OEi7RYq6WcE2OWI9SacEOUe2RIi8TwBvbz==/5a1c7b3d54d505.1888593915118159973475.png"/></div> */}
            {/* <div className="row-item">Datum</div> */}
            {/* <div className="row-item">Anlagentyp</div> */}

          </div>
          <div>
            {stocks.map((stock) => (

                <div className='table-row' key={stock.isin}>
                  <div className="row-itemsmall" onClick={() => updateWatchlist(stock.isin)}>{getWatchlist(stock.isin)}</div>
                  <div className="row-item2">{stock.name}</div>
                  <div className="row-item">{stock.isin}</div>
                  <div className="row-item">{stock.type}</div>
                  <div className="row-itemsmall">{stock.stockprice}€ </div>
                  <div className="row-item4" onClick={() => setButtonPopup(true)} >{getLineGraph(stock.isin)}</div> {/** */}
                  <div className="row-item"> <div style={{color: getGrowthRateColor(stock.growthrate) }}> {getGrowthRate(stock.growthrate)}</div> </div>
                  <div className="row-sub-container">
                    <div className="row-item"><label htmlFor="name">Anzahl:</label></div>
                    <div className="row-item"><input  min="1" value={quantityMap.get(stock.isin)} id={'quantity'+stock.isin} name="quantity" onChange={e => setQuantity(stock.isin ,Number.parseInt(e.target.value))} type="number" style={{width: '70px'}} /></div>
                  </div>
                  <div className='row-item'><button type="button" onClick={() => PurchaseStock(stock)} >Kaufen</button></div>
                  

                  <Popup trigger={buttonPopup} setTrigger={setButtonPopup}>
                    {console.log(stock.name)}
                    <h3>{stock.name} - ISIN: {stock.sv_isin}</h3>
                    <PopupContainer>  
                      <p>Anlagetyp: {stock.type} </p>
                      <p> Wert: {stock.stockprice}€</p>
                      <p>Trend: <span style={{color: getGrowthRateColor(stock.growthrate) }}>{getGrowthRate(stock.growthrate)}</span></p>
                    </PopupContainer>
                      <div>{getLineGraph(stock.sv_isin)} </div> 
                  </Popup> 

                



                  <ToastContainer position="top-center"
                      autoClose={3000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                      theme="light"
                      style={{ width: '80%', zIndex: 1000, top: 100}} />
                </div>
                  
              ))} 
              </div>
            </div>
          </div>
        </div>
      </div>
      </>
    )
  }


export default GetDataStocks;
