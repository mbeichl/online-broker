import { Request, Response } from "express";
import purchasesModel from "./model";


type Transaction = {
    userid: number,
    s_isin: string,
    type: string,
    date: string,
    price: number,
    quantity: number;
  };

const purchasesController = {

    async getPurchases(request: Request, response: Response): Promise<void> {
        const userid = parseInt(request.params.userid);
        const purchases = await purchasesModel.getPurchases(userid);
        response.json(purchases);
    },

      /**
           * DEPRECATED!!!
           */
    async buy (request: Request, response: Response): Promise<void> {
       
    },
    async getPurchasedQuantity(request: Request, response: Response): Promise<void> {
        const userid = parseInt(request.params.userid);
        const purchases = await purchasesModel.getPurchasedQuantity(userid);
        response.json(purchases);
    },
    async getAllValuesToISIN(request: Request, response: Response): Promise<void> {
        // const isin = request.params.isin;
        const value = await purchasesModel.getAllValuesToISIN();
        response.json(value);
    },
    async sell(request: Request, response: Response): Promise<void>{
        const transaction = {
            userid: parseInt(request.body.userid),
            s_isin: request.body.s_isin,
            type: request.body.type,
            date: request.body.date,
            price: parseFloat(request.body.price),
            quantity: parseInt(request.body.quantity),
        }
        console.log(transaction);
        await purchasesModel.sell(transaction);
        response.send('true')

    },
    async test(request: Request, response: Response): Promise<void> {
        response.json(`"test": "test"`);
    }

};

export default purchasesController;

