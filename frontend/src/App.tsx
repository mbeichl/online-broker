import React, { createContext, useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route  } from 'react-router-dom';
import Home from './pages';
import SigninPage from './pages/signin';
import RegisterPage from './pages/register';
import OverviewPage from './pages/overview';
import PortfolioPage from './pages/portfolio';
import StocksPage from './pages/stocks';
import AccountPage from './pages/account';
import WatchlistPage from './pages/watchlist';
import UserProvider from './UserProvider';
import NotFound from './pages/notfound';
import StocksPastPage from './pages/stockPastInvest';


const App: React.FC = () => {

   //Daily update
   React.useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/insertStockValues`, {
      method: 'POST'
    })
  
    document.title = "online-broker";
  },[]);
  debugger;

        localStorage.setItem('insertedGrowthrates', "false");
  console.log(localStorage.getItem('insertedGrowthrates'))


  return (
    <>
    <UserProvider>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} >  </Route>
          <Route path="/signin" element={<SigninPage />} >  </Route>
          <Route path="/register" element={<RegisterPage />} >  </Route>
          <Route path="/overview"  element={<OverviewPage  />}> </Route>
          <Route path="/stocks" element={<StocksPage />} > </Route>
          <Route path="/portfolio" element={<PortfolioPage />}>  </Route>
          <Route path="/watchlist" element={<WatchlistPage />}  />
                    <Route path="/stocks-past-invest" element={<StocksPastPage />} > </Route>


          {/* <Route path="/account" element={<AccountPage />}  /> */}
          <Route path='*'  element={<NotFound />}  />
        </Routes>
      </Router>
    </UserProvider>
    </>

  );
};

export default App;
