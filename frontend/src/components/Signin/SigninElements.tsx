import styled from "styled-components";
import { Link } from 'react-router-dom'
import { Link as LinkR } from 'react-router-dom'


export const Container = styled.div`
    /* min-height: 692px; */
    /* position: fixed; */
    /* bottom: 0;
    left: 0;
    top: 0;
    right: 0;
    z-index: 0; */
    /* overflow: hidden; */
    min-width: 100vw;
    height: 100vh;
    background: linear-gradient(
        108deg,
        rgba(1,147,86,1) 0%,
        rgba(10, 201, 122, 1) 100%
    ); 

`;

export const FormWrap = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    /* justify-content: center;  */
    //
    /* padding: 0 24px; */

     @media screen and (max-width: 400px) {
        height: 80%;
    } 
`; 

export const IconContainer = styled.div`
    display: flex;
    justify-content: space-between;
    height: 80px;
    z-index: 1;
    /* width: 100%; */
    padding: 0 24px;
`


export const Icon = styled(Link)`
    color: #fff;
    justify-self: flex-start;
    cursor: pointer;
    font-size: 1.5rem;
    display: flex;
    align-items: center;
    margin-left: 60px;
    font-weight: bold;
    text-decoration: none;
    width: 400px;
    
    
    /* margin-left: 32px;
    margin-top: 32px;
    text-decoration: none;
    color: #fff;
    font-weight: 700;
    font-size: 32px;

    @media screen and (max-width: 480px) {
        margin-left: 16px;
        margin-top: 8px;
    } */
`;


export const FormContent = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
/*
    @media screen and (max-width: 480px) {
        padding: 10px;
    } */
`;

export const Form = styled.form`
     background: #010101;
    max-width: 400px;
    height: auto;
    width: 100%;
    z-index: 1;
    display: grid;
    margin: 0 auto; 
    padding: 15px 30px 15px 30px;
    /* padding: 80px 32px; */
     border-radius: 4px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.9);
/*
    @media screen and (max-width: 400px) {
        padding: 32px 32px;
    } */
`

export const FormH1 = styled.h1`
    margin-bottom: 40px;
    color: #fff;
    font-size: 20px;
    font-weight: 400;
    text-align: center;
`;




export const FormLabel = styled.label`
    margin-bottom: 8px;
    font-size: 14px;
    color: #fff;
`;


export const FormInput = styled.input`
    padding: 16px 16px;
    margin-bottom: 32px;
    border: none;
    border-radius: 4px;
`;

export const FormButtonContainer = styled.div`
    display: flex;
    width: 100%;
`

export const FormButton = styled.button`
    background: #01bf71;
    padding: 16px 0;
    border: none;
    border-radius: 4px;
    color: #fff;
    font-size: 20px;
    cursor: pointer; 
    width: 100%;
    text-align: center;
    text-decoration: none;

    &:hover{
        transition: all 0.2s ease-in-out;
        background: #fff;
        color: #010606;
    }
`;

export const Text = styled.span`
    text-align: center;
    margin-top: 24px;
    color: #fff;
    font-size: 24px; 
`




export const Nav = styled.nav`
    /* background: ${(scrollNav) => (scrollNav ? 'rgba(1,147,86,1) 0%' : 'transparent')}; */
    background: transparent;
    height: 80px;
    margin-top: -80px;
    display: flex;
    justify-content: left;
    align-items: center;
    font-size: 1rem;
    position: sticky;
    top: 0;
    z-index: 10;

    @media screen and (max-width: 960px){
        transition: 0.8s all ease;
    }
`

export const NavbarContainer = styled.div`
    display: flex;
    justify-content: space-between;
    height: 80px;
    z-index: 1;
    width: 100%;
    padding: 0 24px;
`

export const NavLogo = styled(LinkR)`
  color: #fff;
  justify-self: flex-start;
  cursor: pointer;
  font-size: 1.5rem;
  display: flex;
  align-items: center;
  margin-left: 60px;
  font-weight: bold;
  text-decoration: none;
  width: 400px;
`

