export type User = {
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    password: string;
    accountvalue: number;
    portfoliovalue: number;
    
}