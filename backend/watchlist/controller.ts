import { Request, Response } from "express";
import watchlistModel from "./model";


type Transaction = {
    userid: number,
    s_isin: string,
    type: string,
    date: string,
    price: number,
    quantity: number;
  };

const watchlistController = {

    async getWatchlist (request: Request, response: Response){
        const userid = parseInt(request.params.userid);
        const watchlist = await watchlistModel.getWatchlist(userid);
        
        if (watchlist) {
            response.send(watchlist);
        } else {
            response.send({});
        }
    },

    async setWatchlist (request: Request, response: Response){
        const userid = parseInt(request.body.userid);
        const isin = request.body.isin;

        await watchlistModel.setWatchlist(userid, isin);
        response.send('OK');

    },


    async deleteWatchlistItem (request: Request, response: Response){
        const userid = parseInt(request.body.userid);
        const isin = request.body.isin;

        await watchlistModel.deleteWatchlistItem(userid, isin);
        response.send('OK');
        //send response
    },

    async getWatchlistIsin (request: Request, response: Response){
        const userid = parseInt(request.params.userid);

        const watchlist = await watchlistModel.getWatchlistIsin(userid);
        response.send(watchlist);
        //send response
    }





};

export default watchlistController;

