import { createConnection } from "mysql2/promise";
import { User } from "./User";

// create the connection to database
const connection = createConnection({
  host: "localhost",
  user: "root",
  password: "testbroker",
  database: "wtbroker",
});

const accModel = {
  async getUser(username: String): Promise<User[]> {
    const [rows] = await (
      await connection
    ).execute("SELECT * FROM `users` WHERE userName = ? LIMIT 1", [username]);
    return rows as User[];
  },

  async getUserByID(userid: number): Promise<User> {
    const conn = await connection;
    const [rows, fields] = await (
      await connection
    ).execute(`SELECT * FROM users WHERE id=${userid}`);
    return (rows as User[])[0];
  },


  async deleteAccount(userid: number): Promise<void> {
    try {
      await (
        await connection
      ).query("DELETE FROM `purchases` WHERE userid=?", [userid]);
      await (
        await connection
      ).query("DELETE FROM `users` WHERE userid=?", [userid]);
    } catch (e) {
      throw e;
    }
  }
};

export default accModel;
