import {
  PortfolioContainer,
  SummaryWrapper,
  SummaryH1,
} from "./StocksElements";
import GetDataStocks from "./getDataStocks";
import "../style.css";

const Stocks = () => {
  return (
    <>
      <PortfolioContainer>
        <img
          className="background"
          src="https://asset.museum-digital.org//media/800/westfalen/images/38/20411-00002180_k_65389674_a/1-dollar-schein/1-dollar-schein-20411.jpg"
        ></img>

        <SummaryWrapper>
          <SummaryH1>Börse</SummaryH1>
        </SummaryWrapper>

        <GetDataStocks />
      </PortfolioContainer>
    </>
  );
};

export default Stocks;
