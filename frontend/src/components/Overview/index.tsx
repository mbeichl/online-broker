import React from "react";
import {
  OverviewContainer,
  ImgBg,
  OverviewContent,
  OverviewH1,
  OverviewP,
} from "./OverviewElements";
import Background from "../../videos/background_overview.jpg";
import { UserContext, UserContextType } from "../../UserProvider";
import { HeroBg } from "../HeroSection/HeroElements";

const Overview = () => {
  const { user } = React.useContext(UserContext) as UserContextType;

  return (
    <>
      <OverviewContainer>
        <HeroBg>
          <ImgBg src={Background}></ImgBg>
        </HeroBg>
        <OverviewContent>
          <OverviewH1>Willkommen zurück, {user.firstName}</OverviewH1>
          <OverviewP> Portfoliowert: {user.portfolio}€</OverviewP>
        </OverviewContent>
      </OverviewContainer>
    </>
  );
};

export default Overview;
