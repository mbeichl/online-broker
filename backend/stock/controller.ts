import { Request, Response } from "express";
import stockModel from "./model";
import { apiModel } from "../api/apiModel";
import { Stock } from "./objects/Stock";
import { Value } from "./objects/Value";
import DateLib from "../hilfsmethoden/DateLib";
import { StockWithValue } from "./objects/StockWithValue";
import purchasesModel from "../purchases/model";
//TODO Import 
// import Kurs from "./Kurs/Kurs";


const stockController = {
  //TODO als RequestResponse Methode schreiben
  async buy(request: Request, response: Response): Promise<void> {

    //prüfen ob Kau-Aufruf von Stocks (jetzt ) oder von pastInvest (Vergangenheitskauf) stammt

    /* #region  Jetzt Kauf */
    if(request.body.days_ago == undefined){

      //kein pastprice => ein jetzt kauf
      var isin: string = request.body.isin;
      var type: string = request.body.type;
      var price: number = request.body.stockprice;
      var quantity: number = request.body.quantity;
      var userid: number = request.body.userid;


      console.log(isin);

      console.log(quantity);
      var success: boolean = await purchasesModel.buy(isin, type, price, quantity, userid);

      if (success == true) {
        response.json(`✅ Successfully bought ${quantity} Stocks"`);
      }
      else {
        response.json(`❌ Money isnt enough, to buy those Stocks"`);
      }
    }
    else{
      debugger;
    var isin: string = request.body.isin;
    var type: string = request.body.type;
    var quantity: number = request.body.quantity;
    var userid: number = request.body.userid;
    var days_ago: number = request.body.days_ago;
    
    var price = (await apiModel.getKurs_X_DaysAgo_Object(isin, days_ago)).stockprice;

    console.log(isin);

    console.log(quantity);
    debugger;
    var success: boolean = await purchasesModel.buyPast(isin, type, price, quantity, userid, DateLib.getDate_X_DaysAgo(days_ago));

    if (success == true) {
      response.json(`✅ Successfully bought ${quantity} Stocks at Price ${price}€  for ${days_ago} days ago => for ${DateLib.getDate_X_DaysAgo(days_ago)}"`);
    }
    else {
      response.json(`❌ No Cheatcards left or Money isnt enough, to buy those Stocks"`);
    } 

  }
  },

  async test(){
    var boo = await stockModel.stockValueWithDateExists("AT00000FACC2","2023-01-10");
    //console.log(boo)
  },


  // /* #region  INSERT */
  // async insertAllStocks() {
  //   var aktienDict: Map<string, Stock> = apiModel.getAktienDictionary();
  //   await stockModel.insertAllStocks(aktienDict);
  //   //   this.insertStockValues();
  // },


  async insertStockValues_10(request : Request, response : Response) {
    if (await stockModel.stockValuesExistOnDay(DateLib.getYesterdaysDate())) {
      //console.log("stockvalues of day " + DateLib.getYesterdaysDate() + " already inserted")
    }
    else {
      //console.log("Inserting the 10 StockValues for " + DateLib.getYesterdaysDate() )
      stockModel.insertStockValues_10();
    }
  },

  
  async insertStockValues_10_PastMonth(request : Request, response : Response) {
      //if(await stockModel.stockValuesCount() > 100)
      //console.log("Inserting the StockValues for the past 15 days");
      stockModel.insertStockValues_Past_Month();
    },
  

  /* #endregion */

  /* #region  GETTER */

  async getAllStocks(request: Request, response: Response): Promise<void> {
    //const stocks = await stockModel.getAll();
    const stocks = await stockModel.getAllStocks();

    response.json(stocks);
  },

  async getAllStocksWithValue(request: Request, response: Response): Promise<void> {
    debugger;
    var stocksWithValue: StockWithValue[] = await stockModel.getStockWithValues();

    //FORMATIERUNG DATE
    //  for(let i = 0; i < stocksWithValue.length; i++){
    //     stocksWithValue[i].currentdate = DateLib.cutDateToOnlyDay(stocksWithValue[i].currentdate);
    //  }

    response.json(stocksWithValue)
  },

  async getStocksValue(isin: string): Promise<number> {
    debugger;
    var value = await apiModel.getKursGestern(isin);
    console.log("value:" + value);
    return value;
  },

  async getStockValueObject(isin: string): Promise<Value> {
    debugger;
    var value: Value = await apiModel.getKursGesternObjekt(isin);
    //console.log("value:" + JSON.stringify(value));
    return value;
  },

  async getGrowthratesJSON(request : Request, response : Response) : Promise<void>{


    var  stocks: StockWithValue[]  = await stockModel.getStockWithValues();

    for(let i = 0; i < stocks.length; i++){
      
      var growthRateString = await stockModel.getGrowthRateString(stocks[i].isin);
      var growthRate :number = await stockModel.getGrowthRate(stocks[i].isin);
      var color = stockModel.getGrowthRateColor(growthRateString);
      
      var responseJson : string = `{
        "isin": "${stocks[i].isin}",
        "value": {
          "growthRateString": "${growthRateString}",
          "growthRate": ${growthRate},
          "color": "${color}"

        }
      }`  
      //console.log(responseJson)
      response.json(JSON.parse(responseJson));
    }
  },

  async getGrowthRate(request: Request, response: Response): Promise<void> {
    response.json(stockModel.getGrowthRate(request.body["isin"]));
  },
  /* #endregion */







  // /* #region  rest */
  // async insertStockValues() {
  //   debugger;
  //   var stockValuesExist: boolean = await !stockModel.stockValuesExistOnDay(DateLib.getYesterdaysDate())
  //   if (!stockValuesExist) {
  //     var aktienDict: Map<string, Stock> = apiModel.getAktienDictionary();



  //     const DictArrayStocks = Array.from(aktienDict, ([key, value]) => ({
  //       key,
  //       value,
  //     }));

  //     // for (let i = 0; i < 10; i++) {
  //     //   stockModel.insertStockValue(DictArrayStocks[i].key);
  //     // }
  //     // function delayedCall(index: number): Promise<void> {
  //     //   return new Promise((resolve, reject) => {
  //     //     setTimeout(() => {
  //     //       stockModel.insertStockValue(DictArrayStocks[index].key)
  //     //       resolve();
  //     //     }, 500);
  //     //   });
  //     // }

  //     // async function insertValues() {
  //     //   for (let i = 0; i < 10; i++) {
  //     //     await delayedCall(i);
  //     //   }
  //     // }
  //     // insertValues();
  //   }
  //   else {
  //     console.log("Stockvalues already inserted!")
  //   }
  // }
  /* #endregion */
}





export default stockController;


