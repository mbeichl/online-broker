import styled, { css } from 'styled-components'



export const PortfolioContainer = styled.div`
    background: #f8f8f8;
    padding: 20px 40px 5px 30px;
    display: flow-root;
    /* justify-content: flex-start;
    align-items: flex-start; */
    position: relative;
    margin-top: 80px;

    /* height: 800px; */

    z-index: 1; */

    :before {
        /* content: '';
        position: absolute;
        z-index: 2;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0; */
        /* background: linear-gradient(180deg, rgba(0,0,0,0.2) 0%, 
        rgba(0,0,0,0.6) 100%), linear-gradient(180deg, rgba(0,0,0,0.2) 0%, transparent 100%); */
       
    }
`


export const SummaryWrapper = styled.div`
    z-index: 2;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    //position: absolute;
    padding: 8px 24px;
    /* max-width: 1200px; */
    
`

export const SummaryH1 = styled.h1`
    color: #000;
    font-size: 42px;
    text-align: start;

    /* @media screen and (max-width: 768px){
        font-size: 38px;
    }

    @media screen and (max-width: 480px){
        font-size: 30px;
    } */
`

export const SummaryH2 = styled.h1`
    color: #000;
    font-size: 36px;
    text-align: start;

    /* @media screen and (max-width: 768px){
        font-size: 38px;
    }

    @media screen and (max-width: 480px){
        font-size: 30px;
    } */
`

export const DetailedWrapper = styled.div`
    z-index: 3;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    //position: absolute;
    padding: 8px 24px;
    /* max-width: 1200px; */
    
`


export const DetailedRowWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    
    height: 200px;
    width: 100%;
    //position: absolute;
    padding: 8px 24px;
    /* max-width: 1200px; */
`

export const DetailedRowHeader = styled.h1`
    color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px;
`

export const DetailedRowWert = styled.h1`
    color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px;
`


export const DetailedRowGraph = styled.div`
`





///TEST
export const DivContainer = styled.div`
    background-color: aqua;
`


export const DivWrapper = styled.div`
    background-color: blue;

`

export const DivName = styled.p`
    background-color: brown;
    float: inline-start;
`

export const DivISIN = styled.p`
    float: inline-start;
    background: red;
`
