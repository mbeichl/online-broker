import {
  PortfolioContainer,
  SummaryWrapper,
  SummaryH1,
  Paragraph,
  drei_kaufe,
} from "./StocksElements";
import GetDataStocks from "./getDataStocks";
import "./style.css";

const Stocks = () => {
  return (
    <>

    <div className="wrapperpastinvest" > 

      <div className="containerpastinvest">

         <div className="dollar-boxnew" >
            <SummaryH1>Past Shop</SummaryH1>
            <h1 className="headerPastInvest">
              3 Mal Pro tag können sie hier als zeitreisender eine Aktie kaufen.
              Sie kaufen heute die Aktie zum Wert von for X Tagen
            </h1>
          </div>

          <div className="schweizer-frankenew">
            <img width="350" src="https://sportshub.cbsistatic.com/i/2021/03/16/8b3ec7bd-8611-479b-9efc-ab340593588f/back-to-the-future-trilogy-1122951.jpg"></img> 
          </div>

          <div className='dollar-box-3malnew' > 
            <h1 className="headerPastInvest" style={{fontSize: '28px'}}>3 Käufe in die Vergangenheit </h1>
          </div>

        </div>
          
        <GetDataStocks />
    </div>


      {/* <PortfolioContainer>
        {/* <HeroBg>
            <ImgBg src="https://bullsonwallstreet.com/wp-content/uploads/2018/03/03425_windowtogothamcity_2560x1600.jpg" ></ImgBg>
        </HeroBg> */}
        {/* https://sportshub.cbsistatic.com/i/2021/03/16/8b3ec7bd-8611-479b-9efc-ab340593588f/back-to-the-future-trilogy-1122951.jpg *

        <SummaryWrapper>
          
          <div className="dollar-box" >
            <SummaryH1>Past Shop</SummaryH1>
            <Paragraph>
              3 Mal Pro tag können sie hier als zeitreisender eine Aktie kaufen.
              Sie kaufen heute die Aktie zum Wert von for X Tagen
            </Paragraph>
          </div>

          <div className="schweizer-franke">
            <img width="350" src="https://sportshub.cbsistatic.com/i/2021/03/16/8b3ec7bd-8611-479b-9efc-ab340593588f/back-to-the-future-trilogy-1122951.jpg"></img> 
          </div>

          <div className='dollar-box-3mal'> {/*className='dollar-box-3mal' *
            <Paragraph>3 Käufe in die Vergangenheit </Paragraph>
          </div>

          
        </SummaryWrapper>

        <GetDataStocks />
      </PortfolioContainer> */}
    </>
  );
};

export default Stocks;
