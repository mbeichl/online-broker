import React, { useEffect, useState } from "react";
import { StockWithValue } from "./Stock";
import { Line } from "react-chartjs-2";
import "reactjs-popup/dist/index.css";
import { IUser, UserContext, UserContextType } from "../../UserProvider";
import "reactjs-popup/dist/index.css";
import { Value } from "../Portfolio";
import { ToastContainer, toast } from "react-toastify";

const GetDataStocks = () => {
  const { user } = React.useContext(UserContext) as UserContextType;
  let userdata: IUser = user;

  const [stocks, setStocks] = useState<StockWithValue[]>([]);
  const [buttonPopup, setButtonPopup] = useState(false);
  const [cheatcard, setCheatcard] = useState<string>("");

  const [value, setValues] = useState<Value[]>([]);

  //die Quantitys die in input eingetragen werden , werden als Mamp Key: ISIN, Value: quantity gespeichert und angezeigt
  var quantityMap: Map<string, number> = new Map<string, number>();

  function setQuantity(isin: string, quantity: number) {
    quantityMap.set(isin, quantity);
  }

  //die days_ago die in input eingetragen werden , werden als Mamp Key: ISIN, Value: days_ago gespeichert und angezeigt
  var days_agoMap: Map<string, number> = new Map<string, number>();

  function setDays_agoMap(isin: string, days_ago: number) {
    days_agoMap.set(isin, days_ago);
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/getAllStocks`, {
      headers: {
        "Content-Type": "application/json",
        //authorization: `Bearer ${localStorage.getItem('tocken')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => setStocks(data));

    setValues([]);
    fetch(`${process.env.REACT_APP_BACKEND_URL}/purchases/stocks`, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => setValues(data));
  }, []);

  async function PurchaseStock(stockWithValue: StockWithValue) {
    console.log("einfsach cliiicken");
    var success: string = "";
    var days_ago = days_agoMap.get(stockWithValue.isin);
    //type cheat: wird benutzt um normal gekaufte aktien von mit Past Cheat geakluften in portfolio zu unterscheiden
    var body: string = `{
        "isin": "${stockWithValue.isin}",
        "name": "${stockWithValue.name}",
        "type": "cheat",
        "stockprice": ${stockWithValue.stockprice},          
        "currentdate": "${stockWithValue.currentdate}",          
        "growthrate": "${stockWithValue.growthrate}",
        "quantity": ${quantityMap.get(stockWithValue.isin)},
        "userid": ${userdata.id} ,
        "days_ago": ${days_ago}
        
      }`;

    console.log(JSON.stringify(body));

    

    fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/buy`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*", //<---- Add this
        //authorization: `Bearer ${localStorage.getItem('tocken')}`,
      },
      body: body,
    })
      .then((response) => response.json())
      .then((data) => toast(data)  );
  }

  function getGrowthRateColor(growthrate: string): string {
    var growthrateColor: string;

    if (growthrate.at(0) == "-") {
      return "red";
    }

    if (growthrate.at(0) == "+") {
      return "green";
    }
    return "grey";
  }

  function getGrowthRate(growthrate: string): string {
    if (growthrate.at(0) == "-") {
      return growthrate + " " +  "% " + "\u2b07";
    }

    if (growthrate.at(0) == "+") {
      return growthrate + " " + "% " + "\u2B06" ;
    }
    return growthrate + "%";
  }

  function getLineGraph(isin: String) {
    const arr: Value[] = [];
    let dateString: string;

    value.map((i) => {
      if (i.sv_isin === isin) {
        dateString = i.currentdate.substring(0, 10);
        i.currentdate = dateString;
        arr.push(i);
      }
    });

    //arr.sort(compare)

    const dataSet = {
      labels: arr.map((item) => item.currentdate),
      datasets: [
        {
          data: arr.map((item) => item.stockprice),
          borderColor: "rgb(255, 99, 132)",
          backgroundColor: "rgba(255, 99, 132, 0.5)",
        },
      ],
    };

    const option = {
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        scales: {
          scales: {
            x: [
              {
                ticks: {
                  display: false,
                },
              },
            ],
            yAxes: [
              {
                display: false,
              },
            ],
          },
        },
      },
    };
    return <Line options={option} data={dataSet} />; //<Line data={}/>
  }

  if (!stocks.length) {
    return <div>didn't get a stock</div>;
  }

  return (
    <>
      <div className="wrapper">
        <div className="header">
        </div>
        <div className="body">
          <div className="main-container">
            <div className="table-container">
              <div className="table-row headinger">
                <div className="row-item">Name</div>
                <div className="row-item">ISIN</div>
                <div className="row-item">Anlagetyp</div>
                <div className="row-item">Kurs</div>
                <div className="row-item">Graph</div>
                <div className="row-item">Trend</div>
                <div className="row-item">Kaufen</div>
                <div className="row-item"></div>

            {/* <div className="row-item"><img width="30" height="30" src="https://png2.cleanpng.com/sh/b41f40dac46116ae1a756a5d8e14b33d/L0KzQYi4UcI4N5c5UZGAYUHmR7O6hMU1bGY1TZC6OEi7RYq6WcE2OWI9SacEOUe2RIi8TwBvbz==/5a1c7b3d54d505.1888593915118159973475.png"/></div> */}
            {/* <div className="row-item">Datum</div> */}
            {/* <div className="row-item">Anlagentyp</div> */}

          </div>
          <div>
            {stocks.map((stock) => (

                <div className='table-row' key={stock.isin}>
                  <div className="row-item">{stock.name}</div>
                  <div className="row-item">{stock.isin}</div>
                  <div className="row-item">{stock.type}</div>
                  <div className="row-item">{stock.stockprice}€ </div>
                  <div className="row-item">{getLineGraph(stock.isin)}</div>
                  <div className="row-item"> <div style={{color: getGrowthRateColor(stock.growthrate) }}> {getGrowthRate(stock.growthrate)}</div> </div>
                  {/* <div className="row-item">{stock.currentdate}</div> */}
                  
                  <div className="row-sub-container">
                  <div className="row-item"><label htmlFor="name">Anzahl:</label></div>
                    <div className="row-item"><input value={quantityMap.get(stock.isin)} id="quantity" name="quantity" onChange={e => setQuantity(stock.isin ,Number.parseInt(e.target.value))} type="number" /></div>
                    <div> Kaufe Aktie vor <input style={{width: 30}} value={days_agoMap.get(stock.isin)} id="quantity" name="quantity" onChange={e => setDays_agoMap(stock.isin ,Number.parseInt(e.target.value))} type="number" /> {" "} Tagen </div>

                 
                  </div>
                  <div className='row-item'><button type="button" onClick={() => PurchaseStock(stock)} >Kaufen</button></div>
                  
                  <ToastContainer position="top-center"
                      autoClose={3000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                      theme="light"
                      style={{ width: "80%", zIndex: 1000, top: 100 }}
                    />

                    {/* <div className="row-item">
                    <input value={quantity} id="quantity" name="quantity" onChange={e => setQuantity(e.target.value)} type="number" />; 
                    <button >Purchase
                    </button>
                     <label>Quantity</label> 
                  </div>   */}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

// table

// Spalte1: Name
// spalte2: kurs
// spalte 3 trend veränderung
// palte 4 kurswert

export default GetDataStocks;
