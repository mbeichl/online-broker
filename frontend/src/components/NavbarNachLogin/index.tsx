import React from 'react'
import { IconContext } from 'react-icons/lib'
import { animateScroll as scroll } from 'react-scroll'
import { IUser, UserContext, UserContextType } from '../../UserProvider'
import { Nav, NavbarContainer, NavLogo, MobileIcon, NavMenu, NavItem, NavLinks, NavBtn, NavBtnLink,NavItemWallet , NavBarElement, Navdiv } from './NavbarNachLoginElements'

// import { useHistory } from "react-router-dom";


const NavbarNachLogin = () => {
    const { saveUser } = React.useContext(UserContext) as UserContextType;
    
  const { user } = React.useContext(UserContext) as UserContextType;
  let userdata:IUser = user;
//   let history = useHistory();

    function onLogout(){
        const newUser: IUser = { 
            id: 0,
            firstName: '',
            lastName: '',
            userName: '',
            email: '',
            password: '',
            accountvalue: 0,
            portfolio: 0,
            cheatcards: 3
        };

        localStorage.removeItem('token');
        sessionStorage.removeItem('token');
            // history.push({
            //     pathname: '/details',
            //     state: {
            //       from: 'profile',
            //     }
            //   });
    }


      const toogleHome = () => {
        scroll.scrollToTop()
      }

  return (
    <>
    <IconContext.Provider value={{ color: '#fff'}}>
         <Nav > {/*scrollNav={scrollNav} */}
            <NavbarContainer>
                <NavLogo to='/overview' >ONLINE-BROKER</NavLogo>
                {/* <MobileIcon onClick={toggle}>
                    <FaBars />
                </MobileIcon> */}
                <NavMenu>
                    <NavItemWallet>
                        <Navdiv > Portfolio: {userdata.portfolio} € </Navdiv>
                    </NavItemWallet>
                    <NavItem>
                        <NavLinks to='/stocks' >Stocks</NavLinks>
                    </NavItem>
                    <NavItem>
                        <NavLinks to='/portfolio' >Portfolio</NavLinks>
                    </NavItem>
                    <NavItem>
                        <NavLinks to='/watchlist' >Watchlist</NavLinks>
                    </NavItem>
                    <NavItem>
                        <NavLinks to='/stocks-past-invest' >Cheat Past Invest</NavLinks>
                    </NavItem>
                    {/* <NavItem>
                        <NavLinks to='/account' >Account</NavLinks>
                    </NavItem> */}
                    <NavItemWallet>
                        <Navdiv > Wallet: {userdata.accountvalue} €  {/*<img width="60" height="30" src="https://asset.museum-digital.org//media/800/westfalen/images/38/20411-00002180_k_65389674_a/1-dollar-schein/1-dollar-schein-20411.jpg"></img> */}</Navdiv>
                    </NavItemWallet>
                </NavMenu>
                <NavBtn>
                    <NavBtnLink to='/' onClick={onLogout}>Logout</NavBtnLink>
                </NavBtn>
            </NavbarContainer>
            
        </Nav>
        </IconContext.Provider>
    </>
  )
}

export default NavbarNachLogin