import styled from "styled-components";

export const OverviewContainer = styled.div`
  background: #0c0c0c;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
  height: 100vh;
  position: relative;
  z-index: 1;

  /* :before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: linear-gradient(180deg, rgba(0,0,0,0.2) 0%, 
        rgba(0,0,0,0.6) 100%), linear-gradient(180deg, rgba(0,0,0,0.2) 0%, transparent 100%);
        z-index: 2;
    } */
`;

export const BackgroundDiv = styled.div`
  height: 100%;
  width: 100%;
`;

export const BgIMG = styled.img`
  height: 100%;
  width: 100%;
  opacity: 0.2;
  z-index: 99;
  background: linear-gradient(
      180deg,
      rgba(0, 0, 0, 0.2) 0%,
      rgba(0, 0, 0, 0.6) 100%
    ),
    linear-gradient(180deg, rgba(0, 0, 0, 0.2) 0%, transparent 100%);
`;

export const ImgBg = styled.img`
  width: 100%;
  height: 100%;
  opacity: 0.2;
  /* -o-object-fit: cover; */
  object-fit: cover;
  background: #232a34;
`;

export const Content = styled.div`
  border-radius: 1px;
  border-color: grey;
  background-color: #f8f8f8;
  justify-content: center;
  align-items: center;
`;

export const OverviewContent = styled.div`
  z-index: 3;
  /* max-width: 1200px; */
  position: absolute;
  /* padding: 8px 24px; */
  padding: 62px 100px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #ededed;
  border-radius: 10px;
  /* border: 10px red; */
`;

export const OverviewH1 = styled.h1`
  color: #000;
  font-size: 48px;
  text-align: center;

  /* @media screen and (max-width: 768px){
        font-size: 40px;
    }

    @media screen and (max-width: 480px){
        font-size: 32px;
    } */
`;

export const OverviewP = styled.p`
  margin-top: 24px;
  font-size: 24px;
  text-align: center;
  color: #000;
`;
