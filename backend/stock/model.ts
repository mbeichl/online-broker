
import { createConnection } from 'mysql2/promise';
import { Stock } from './objects/Stock';
import { Value } from './objects/Value'
import DateLib from '.././hilfsmethoden/DateLib';
import { apiModel } from '../api/apiModel';
import { StockWithValue } from './objects/StockWithValue';
import { is } from 'date-fns/locale';

// create the connection to database
const connection = createConnection({
    host: 'localhost',
    user: 'root',
    password: 'testbroker',
    database: 'wtbroker',
});




const stockModel = {


    /* #region INSERT METHODS   */
    
    async insertAllStocks(aktienDictionary: Map<string, Stock>) {
        //console.log("insertAllStocks");
        const conn = await connection;

        //Clear Table first (this is method is just a init bulk insert!)
        conn.query(
            `delete from stocks;`
        )

        aktienDictionary.forEach(async (value: Stock, key: string) => {
            console.log("foreach");

            conn.query(
                'INSERT INTO stocks (isin, name, titel, type) VALUES (?, ?, ?, ?)',
                [value.isin, value.name, value.titel, value.type]
            )

            //console.log(`insrted stock: ${value.isin},  ${value.name} into stockdb`);

            // this.insertStockValue(value.isin);
            //}
        });

    },

    /**
    * mehr als 10 API Aufrufe hintereinander lösen Probleme mit dem RateLimit aus!!!
    */
    async insertStockValues_10() {
        var stockArray: Stock[] = await this.getAllStocks();
        //= ["DE0005190003", "IE00BZ12WP82", "DE0007236101", "DE0007164600", "DE0007164600", "DE0007664039", "DE0005552004", "DE0008404005", "DE000BASF111", "DE000BASF111"]
        ;
        for (let i = 0; i < 10; i++) {
            var val1 = await apiModel.getKursGesternObjekt(stockArray[i].isin)
            //console.log(val1);
            this.insertStockValue(val1);
        }

    },

    async insertStockValues_Past_Month() {
        var stockArray: Stock[] = await this.getAllStocks();
        //= ["DE0005190003", "IE00BZ12WP82", "DE0007236101", "DE0007164600", "DE0007164600", "DE0007664039", "DE0005552004", "DE0008404005", "DE000BASF111", "DE000BASF111"]
        ;

        for (let d = 4; d < 15; d++) {
            setTimeout(() => console.log("timeout 1000"),1000);
            for (let i = 0; i < 10; i++) {
                var val1 = await apiModel.getKurs_X_DaysAgo_Object(stockArray[i].isin, d)
                //console.log(val1);
                this.insertStockValue(val1);
            }
        }
    },

    async insertStockValues_X_Days_ago(days_ago: number) : Promise<void>{
        var stockArray: Stock[] = await this.getAllStocks();
        //= ["DE0005190003", "IE00BZ12WP82", "DE0007236101", "DE0007164600", "DE0007164600", "DE0007664039", "DE0005552004", "DE0008404005", "DE000BASF111", "DE000BASF111"]
        ;

        for (let i = 0; i < 10; i++) {
            var val1 = await apiModel.getKurs_X_DaysAgo_Object(stockArray[i].isin, days_ago)
            //console.log(val1);
            this.insertStockValue(val1);
        }

    },


    /**
      * fügt StockValue von isin in DB ein
      * @returns true wenn erfolgreich, false wenn nicht erfolgriech ( kurs existiert nicht API)
      */
    async insertStockValue(stockValue: Value): Promise<boolean> {

        const conn = await connection;

        //const stockValue: Value = await apiModel.getKursGesternObjekt(isin);
        var stockExists: boolean = await this.stockValueWithDateExists(stockValue.sv_isin, stockValue.currentdate);
        if (stockValue.stockprice != -1 && !stockExists) {
            var query = `
                        INSERT IGNORE INTO stockvalues (sv_isin, stockprice, currentdate) VALUES 
                ('${stockValue.sv_isin}', ${stockValue.stockprice}	, '${stockValue.currentdate}');`

            conn.query(
                query
            )

            //console.log(`insrted stockValue: '${stockValue.sv_isin}', ${stockValue.stockprice}	, '${stockValue.currentdate}'`);
            return true;
        }
        else {
            //console.log(`WARNING: Error in serting ${stockValue.sv_isin}: ${stockValue.sv_isin}`);
            return false;
        }

    },

    /* #endregion INSERT METHODS */
    /* #endregion */


    /* #region  GETTER */

    async getAllStocks() {
        const conn = await connection;

        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * from stocks;`);

        const stocks: Stock[] = rows as Stock[];
        return stocks;
    },
    /**
           * @return Growthrate zwischen gestrigem Tag und vorgestrigem Tag in Prozent(Gestern = now - 3)
           */
    async getGrowthRate(isin: string): Promise<number> {
        debugger;
        const conn = await connection;
        const [rows, fields] = await (
            await connection
        ).execute(`select * from stockvalues where sv_isin='${isin}' order by currentdate desc;`);

        const stockValues: Value[] = rows as Value[];
        var growthRate: number;
        var growthRateRounded: number;

        if (await this.OtherValueExist(isin)) {
            var division: number = (stockValues[0].stockprice / stockValues[1].stockprice) * 100
            growthRate = division - 100;

            growthRateRounded = Math.round((growthRate + Number.EPSILON) * 100) / 100
        }
        else {
            growthRateRounded = 0.00;
        }
        //console.log("Growthrate: " + growthRateRounded);
        return growthRateRounded;
    },

    async getGrowthRateString(isin: string): Promise<string> {

        var growthRateString: string;

        var growthRate: number = await this.getGrowthRate(isin);

        if (growthRate > 0) {
            growthRateString = "+" + growthRate.toString();
        }
        else {
            growthRateString = growthRate.toString();
        }

        return growthRateString;
    },

    getGrowthRateColor(growthrate: string): string {
        var growthrateColor: string;
    
        if (growthrate.at(0) == '-') {
          return 'red';
        }
    
        if (growthrate.at(0) == '+') {
          return 'green';
        }
        return 'grey';
    
        }
    ,
    /**
       * @return Kurs/Preis von stock
       */
    async getStockValueObject(stock: Stock): Promise<Value> {
        const conn = await connection;
        const yesterdaysDate: String = DateLib.getYesterdaysDate();
        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * FROM stockvalues where currentdate='${yesterdaysDate}' AND sv_isin='${stock.isin}';`);

        const stockValues: Value[] = rows as Value[];
        return stockValues[0];
    },

    /**
     * @return gibt sämtliche Objekte mit sämtlichen Kursen von jeder Aktie zurück
     */
    async getStockWithValues(): Promise<StockWithValue[]> {
        const conn = await connection;
        const yesterdaysDate: String = DateLib.getYesterdaysDate();
        var my_query = ` SELECT s.isin, s.name, s.type, sv.sv_isin, sv.stockprice, sv.currentdate FROM stockvalues sv, stocks s where sv.sv_isin = s.isin AND currentdate='${yesterdaysDate}';`;
        const [rows, fields] = await (
            await connection
        ).execute(my_query);

        const stockWithValues: StockWithValue[] = rows as StockWithValue[];
        for (let i = 0; i < stockWithValues.length; i++) {
            stockWithValues[i].growthrate = await this.getGrowthRateString(stockWithValues[i].isin);
        }
        debugger;

        return stockWithValues;
    },

    /**
 * @return gibt zu jeder Aktie nur das Objekt mit dem aktuellsten Kurs zurück
 */
    async getStockWithValues_OnlyLatest(): Promise<StockWithValue[]> {
        const conn = await connection;
        const yesterdaysDate: String = DateLib.getYesterdaysDate();
        const [rows, fields] = await (
            await connection
        ).execute(` SELECT s.isin, s.name, s.type, sv.sv_isin, sv.stockprice, sv.currentdate 
                        FROM stockvalues sv, stocks s 
                        where sv.sv_isin = s.isin
                        order by currentdate desc;`);

        const stockWithValues: StockWithValue[] = rows as StockWithValue[];
        const stockWithValues_OnlyLatest: StockWithValue[] = rows as StockWithValue[];

        for (let i = 0; i < stockWithValues.length; i++) {
            stockWithValues[i].growthrate = await this.getGrowthRateString(stockWithValues[i].isin);


            //check if Stock already exists => beim ersten (= neuestes durch orderby currentdate desc)
            if (!this.isinExistInArray(stockWithValues_OnlyLatest, stockWithValues[i].isin)) {
                stockWithValues_OnlyLatest[i] = stockWithValues[i];

            }
        }
        return stockWithValues;
    },
    /* #endregion */

    /* #region  HILFSMETHODEN */
    /** 
       * @return gibt boolean zurück, ob es von der Aktie noch einen (weiteren) Value gibt in stockvalues table
       */
    async OtherValueExist(isin: string): Promise<boolean> {
        const conn = await connection;
        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * FROM stockvalues where sv_isin='${isin}'`);

        const stockValues: Value[] = rows as Value[];
        if (stockValues.length > 1) {
            return true;
        }
        else {
            return false;
        }
    },


    //HILFSMETHODEN
    /**
       * @returns returns boolean ob Stockvalues mit unique key isin-date bereits exisiteren
       */
    async stockValueWithDateExists(isin: string, currentdate: string): Promise<boolean> {
        const conn = await connection;
        var query = `SELECT * FROM stockvalues where sv_isin='${isin}' AND currentdate='${currentdate}';`;
        console.log(`QUERY: ${query}`);
        const [rows, fields] = await (
            await connection
        ).execute(query);

        const values: Value[] = rows as Value[];

        if (values.length == 0) {
            return false;
        }
        else {
            return true;
        }
    },

    isinExistInArray(arr: StockWithValue[], isin: string): boolean {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].isin == isin) {
                return true;
            }
        }
        return false;
    },

    /**
     * @return bool if isin already exists in StockDB
     */
    async isin_Exists(isin: string): Promise<boolean> {
        const conn = await connection;

        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * FROM stocks where isin='AT0000634704';`);

        const stocks: Stock[] = rows as Stock[];

        if (stocks.length == 0) {
            return false;
        }
        else {
            return true;
        }
    },
    /**
     * @returns returns boolean ob Stockvalues für date parameter existieren
     */
    async stockValuesExistOnDay(date: string): Promise<boolean> {
        const conn = await connection;
        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * FROM stockvalues where currentdate='${date}';`);

        const values: Value[] = rows as Value[];

        if (values.length == 0) {
            return false;
        }
        else {
            return true;
        }
    },

        /**
     * @returns returns boolean ob Stockvalues für date parameter existieren
     */
        async stockValuesCount(date: string): Promise<number> {
            const conn = await connection;
            const [rows, fields] = await (
                await connection
            ).execute(`SELECT * FROM stockvalues;`);
    
            const values: Value[] = rows as Value[];
                return values.length;
        }
    

    /* #endregion */
};



export default stockModel;
