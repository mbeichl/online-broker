import cors from 'cors';
import { Router } from 'express';
import authController from './controller';

const authRouter = Router();

authRouter.use(cors());

// http://localhost:8080/auth/login
authRouter.post('/login', authController.login);
authRouter.post('/register', authController.register);
authRouter.post('/updateUser', authController.updateUser);
authRouter.post('/:username', authController.getUser);









export default authRouter;

