// export const Stock = class Stock {

// //ID der Aktie
// ISIN: string;

// //etwas abgekürzter mit einer ID
// name: string;

// //ausgeschriebener Name
// titel: string;

// //stock
// type: string;

//   constructor(ISIN : string ,    name: string,      titel: string,     type: string ) {
//     this.ISIN = ISIN;
//     this.name = name;
//     this.titel = titel;
//     this.type = type;
//   }
  
// }
//TYPE ATTRIBUTE IMMER KLEIN!!
  export type Stock = {
    //ID der Aktie
    isin: string;

    //etwas abgekürzter mit einer ID
    name: string;

    titel: string
    //stock
    type: string;
  };

export default Stock;