import { Router } from "express";
import watchlistController from "./controller";

const watchlistRouter = Router();

watchlistRouter.post('/updateWatchlist', watchlistController.setWatchlist);
watchlistRouter.post('/deleteWatchlistItem', watchlistController.deleteWatchlistItem);
watchlistRouter.get('/getWatchlist/:userid', watchlistController.getWatchlistIsin);
watchlistRouter.get('/:userid', watchlistController.getWatchlist);


export default watchlistRouter;




