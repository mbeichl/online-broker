import { secret } from "../constants";
import jwt from "jsonwebtoken";
import { createConnection } from "mysql2/promise";
import { response } from "express";

const bcrypt = require("bcrypt");
const saltrounds = 12;
// create the connection to database
const connection = createConnection({
  host: "localhost",
  user: "root",
  password: "testbroker",
  database: "wtbroker",
});

// model of user data for database
export type UserData = {
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  cheatcards:number;

};

//model of user data for login
type User = {
  username: string;
  password: string;
};

type UserDataComplete = {
  id: number;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  accountvalue: number;
  portfolio: number;
  cheatcards:number;
};

type newData = {
  userid: number;
  accountvalue: number;
  portfoliovalue: number;
};

const authModel = {
  async login(user: User): Promise<string | false> {
    const [rows] = await (
      await connection
    ).execute(
      "SELECT userName, password FROM users WHERE userName = ? LIMIT 1",
      [user.username]
    ); 
    const upwval = rows as User[];
    if(upwval.length == 0){
      return false;
    }
    else{
      var result = bcrypt.compareSync(user.password, upwval[0].password);
    if (result == true) {
      //console.log("passwords match");
      // if ((rows as Array<UserData>).length) {
        const token = jwt.sign({ username: user.username }, secret, {
          expiresIn: "1h",
        });
      return token;
      // }
    }
    else{
    //console.log("error");
   return false;
    } 
  } 
  },
  async register(user: UserData){
    const conn = await connection;
    const [rows] = await (await connection).execute(
      "SELECT userName, password FROM users WHERE userName = ?",
      [user.userName]
    );
    const usrval = rows as UserData[];
    if(usrval.length == 0){
        const hashedpassword = bcrypt.hashSync(
          user.password,
          saltrounds,
          //console.log("password hashed")
        );
       return conn.query(
          "INSERT INTO users (firstName, lastName, userName, email, password, cheatcards) VALUES (?, ?, ?, ?, ?, ?)",
          [
            user.firstName,
            user.lastName,
            user.userName,
            user.email,
            hashedpassword,
            3,
          ]
        );
    }
    else{
      return false;
    }
  },

  async getUserData(username: String): Promise<UserDataComplete[]> {
    const [rows] = await (
      await connection
    ).execute(
      "SELECT id, firstName, lastName, userName, email, password, accountvalue, portfolio FROM `users` WHERE userName = ? LIMIT 1",
      [username]
    );
    return rows as UserDataComplete[];

  },

  async updateUser(data: newData) {
    const [rows] = await (
      await connection
    ).execute("UPDATE users SET accountvalue = ?, portfolio = ? WHERE id = ?", [
      data.accountvalue,
      data.portfoliovalue,
      data.userid,
    ]);
  },
};

export default authModel;
