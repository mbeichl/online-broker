export type Stock = {
    id: number;
    isin: string;
    stockname: string;
    stocktitle: string;
    stocktype: string;
};


export type Stockvalue = {
    userid: number;
    isin: string;
    kaufdatum: Date;
    kaufkurs: number;
    anzahl: number;
    verkaufsdatum: Date;
    verkaufskurs: number;
}


export type StockWithValue = {
    isin: string;
    name: string;
    type: string;
    id: number;
    sv_isin: string;
    stockprice: number;  
    currentdate: string;
    growthrate: string;
  };


  export  type Values = {
    sv_isin: string;
    stockprice: number;
    currentdate: string;
  };

