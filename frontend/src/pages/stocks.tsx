import React from 'react'
import StocksPage from '../components/Stocks/getDataStocks'
import NavbarNachLogin from '../components/NavbarNachLogin'


const Stocks: React.FC = () => {

    return(
        <>
        <NavbarNachLogin />
        <StocksPage />
        {/* <Footer /> */}
        </>
    )
}


export default Stocks;
