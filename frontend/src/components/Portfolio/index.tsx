import React, { useState, useEffect } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import { Value, PopupContainer } from "./PortfolioElements";
import { StockWithValue } from "../Stocks/Stock";
import Popup from "./Popup";
import "./stylePortfolio.css";
import { IUser, UserContext, UserContextType } from "../../UserProvider";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../style.css";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

type PurchaseQuantity = {
  name: string;
  sv_isin: string;
  quantity: number;
};

export type Value = {
  sv_isin: string;
  currentdate: string;
  stockprice: number;
};

type Transaction = {
  userid: number;
  s_isin: string;
  type: string;
  date: string;
  price: number;
  quantity: number;
};

const Portfolio = () => {
  const { user, saveUser } = React.useContext(UserContext) as UserContextType;

  const [purchases, setPurchases] = useState<PurchaseQuantity[]>([]);
  const [stocks, setStocks] = useState<StockWithValue[]>([]);
  const [value, setValues] = useState<Value[]>([]);
  const [buttonPopup, setButtonPopup] = useState(false);
  const [refetch, setRefetch] = useState(0);

  useEffect(() => {
    fetch(
      `${process.env.REACT_APP_BACKEND_URL}/purchases/purchases/${user.id}`,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((data) => setPurchases(data));

    fetch(`${process.env.REACT_APP_BACKEND_URL}/stocks/getAllStocks`, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => setStocks(data));

    setValues([]);
    fetch(`${process.env.REACT_APP_BACKEND_URL}/purchases/stocks`, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => setValues(data));
  }, [refetch]);

  function getTrend(isin: string): string {
    let trend: string = "";
    stocks.map((s) => {
      if (s.isin === isin) {
        trend = s.growthrate;
      }
    });
    if (!trend) {
      return "";
    } else {
      if (trend.at(0) == "-") {
        return trend + " " + "%       " + "\u2b07";
      }

      if (trend.at(0) == "+") {
        return trend + " " + "%       " + "\u2B06";
      }
      return trend + "%";
    }
  }

  function getGrowthRateColor(growthrate: string): string {
    if (growthrate.at(0) == "-") {
      return "red";
    }

    if (growthrate.at(0) == "+") {
      return "green";
    }
    return "grey";
  }

  function getAnlagetype(isin: string): string {
    let type: string = "";
    stocks.map((s) => {
      if (s.isin === isin) {
        type = s.type;
      }
    });
    if (!type) {
      return "";
    } else {
      return type;
    }
  }

  function getKurs(isin: string): string {
    let kurs;
    stocks.map((s) => {
      if (s.isin === isin) {
        kurs = s.stockprice;
      }
    });
    if (!kurs) {
      return "";
    } else {
      return kurs;
    }
  }

  function getLineGraph(isin: String) {
    const arr: Value[] = [];
    let dateString: string;

    value.map((i) => {
      if (i.sv_isin === isin) {
        dateString = i.currentdate.substring(0, 10);
        i.currentdate = dateString;
        arr.push(i);
      }
    });

    const dataSet = {
      labels: arr.map((item) => item.currentdate),
      datasets: [
        {
          data: arr.map((item) => item.stockprice),
          borderColor: "rgb(255, 99, 132)",
          backgroundColor: "rgba(255, 99, 132, 0.5)",
        },
      ],
    };

    const option = {
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        scales: {
          scales: {
            x: [
              {
                ticks: {
                  display: false,
                },
              },
            ],
            yAxes: [
              {
                display: false,
              },
            ],
          },
        },
      },
    };

    return <Line options={option} data={dataSet} />;
  }



  
  function onClickSale(btnName: string, buyedQuantity: number, nameStock: string) {
    //get Value of Input
    let stueck: string = (document.getElementById("inputSale-" + btnName.substring(8)) as HTMLInputElement).value;

    //Check if value is number
    let isnum = /^\d+$/.test(stueck);

    if (isnum) {
      //Check if enough to sell
      let anzahl = parseInt(stueck);

      if (anzahl == 0) {
        toast("❌ Bitte Zahl größer Null eingeben!");
      } else if (anzahl <= buyedQuantity) {
        //alert

        //Get Kurs
        let kurs = getKurs(btnName.substring(8));
        let kursFloat: number = 0;
        if (kurs != "") {
          kursFloat = parseFloat(kurs);
        }

        let dateObj = new Date();
        let month = dateObj.getMonth() + 1;
        let day = dateObj.getDate();
        let year = dateObj.getFullYear();
        let priceKurs = kursFloat * -1;
        let quantityNeg = anzahl * -1;

        let transaction: Transaction = {
          userid: user.id,
          s_isin: btnName.substring(8),
          type: "sell",
          date: year + "-" + month + "-" + day,
          price: priceKurs,
          quantity: quantityNeg,
        };

        let portfoliovalue;
        if (parseFloat((user.portfolio - kursFloat * anzahl).toFixed(2)) < 0.0) {
          portfoliovalue = 0;
        } else {
          portfoliovalue = parseFloat((user.portfolio - kursFloat * anzahl).toFixed(2));
        }
        let accval =
          parseFloat(user.accountvalue.toString()) +
          kursFloat * parseFloat(anzahl.toString());
        let accvalue = parseFloat(accval.toFixed(2));

        const newData = {
          id: user.id,
          accountvalue: accvalue,
          portfolio: portfoliovalue,
        };

        const newUser: IUser = {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          userName: user.lastName,
          email: user.email,
          password: user.password,
          accountvalue: accvalue,
          portfolio: portfoliovalue,
          cheatcards: 3,
        };

        //Update in PurchaseDB
        fetch(`${process.env.REACT_APP_BACKEND_URL}/purchases/sell`, {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(transaction),
        })
          .then((response) => response.json())
          .then((data) => {
            console.log("Success:", data);
          })
          .catch((error) => {
            console.error("Error:", error);
          });

        //Update User in DB
        fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/updateUser`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
          },
          body: JSON.stringify(newData),
        })
          .then((response) => {
            if (response.ok) {
              console.log("Success");
            } else {
              console.log("Error");
            }
          })
          .catch((error) => {
            console.error("Error:", error);
          });
        toast("✅ Verkauft: " + nameStock + " - Anzahl: " + anzahl + "Stk.");

        (document.getElementById("inputSale-" + btnName.substring(8)) as HTMLInputElement).value = "";

        saveUser(newUser);
        setRefetch(Math.random());
      } else {
        //Fehlermeldung - Nicht genug Aktien in Besitz
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "";
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "Nicht genug verfügbar!";
        toast(`❌ Nicht genug Aktien zum Verkauf verfügbar! `);
      }
    } else {
      if (buyedQuantity < 1) {
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "";
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "Anzahl muss mind. 1 sein!";

      } else {
        //Fehlermeldung - Nur Zahlen
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "";
        (document.getElementById("errormsg" + btnName.substring(8)) as HTMLElement).innerHTML = "Bitte nur Zahlen eingeben!";
      }
    }
  }

  if (!purchases.length) {
    // if(anzahlStocksInBesitz == 0){
    return (
      <>
        <div className="wrapper">
          <div className="header">
            <h2>Portfolio</h2>
            <p style={{ textAlign: "center", marginTop: "10rem" }}>
              Noch keine Transaktionen!
            </p>
          </div>
        </div>
      </>
    );
  }
  return (
    <>
    <div className='wrapper' >
      <div className='header'>
        <h2 >Portfolio</h2>
        <p>Portfoliowert: {user.portfolio} €</p>
      </div>
      <div className='body'>

        <div className="main-container">
          <div className="table-container">
            <div className="table-row headinger">
              <div className="row-item2">Name</div>
              <div className="row-item">ISIN</div>
              <div className="row-item">Anzahl</div>
              <div className="row-item">Kurs</div>
              <div className="row-item3">Graph</div>
              <div className="row-item">Trend</div>
              <div className="row-item">Verkaufen</div>
              <div className="row-item"></div>
            </div>
            <div>
              {purchases.map((p) => {
                
                if(p.quantity == 0){
                   return (<></>)
                }else {
                  return (
                
                <div className='table-row' key={p.sv_isin}  >
                  <div className='row-item2'>{p.name}</div>
                  <div className='row-item'>{p.sv_isin}</div>
                  <div className='row-item'>{p.quantity}</div>
                  <div className='row-item'>{getKurs(p.sv_isin)}€</div>
                  <div className='row-item3' onClick={() => setButtonPopup(true)}>{getLineGraph(p.sv_isin)}</div>  {/*  */}
                  <div className='row-item' style={{color: getGrowthRateColor(getTrend(p.sv_isin)) }}>  {getTrend(p.sv_isin)}</div>
                  <div className="row-sub-container">
                      <div className="row-item"><label htmlFor="name">Anzahl:</label></div>
                      <div className="errormsg" id={'errormsg'+p.sv_isin}/>
                      <div className="row-item"><input type="text" className='inputSell' id={'inputSale-'+p.sv_isin} name="inputSale" /></div>
                    </div>
                  <div className='row-item'><button name={'btnSale-'+p.sv_isin} onClick={() => onClickSale('btnSale-'+p.sv_isin, p.quantity, p.name)}>Verkaufen</button></div>

                  <Popup trigger={buttonPopup} setTrigger={setButtonPopup}>
                    <h3>{p.name} - ISIN: {p.sv_isin}</h3>
                    <PopupContainer>
                      <p>Anlagetyp: {getAnlagetype(p.sv_isin)}</p>
                      <p>Anzahl: {p.quantity}</p>
                      <p> Wert: {getKurs(p.sv_isin)}€</p>
                      <p>Trend: <span style={{color: getGrowthRateColor(getTrend(p.sv_isin)),}} >{getTrend(p.sv_isin)}</span></p>
                    </PopupContainer>
                    <div>{getLineGraph(p.sv_isin)} </div>
                  </Popup>

                  <ToastContainer
                    position="top-center"
                    autoClose={3000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                    style={{ width: "80%", top: 100 }}
                  />
                </div>
                );}
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Portfolio;
