import React, { useState, useEffect } from "react";
import "../Portfolio/stylePortfolio.css";
import { ToastContainer, toast } from "react-toastify";
import { Line } from "react-chartjs-2";
import { UserContext, UserContextType, IUser } from "../../UserProvider";
import { Value } from "../Portfolio";
import { Stock, StockWithValue } from "../Stocks/Stock";
import { AiFillStar, AiOutlineStar } from "react-icons/ai";
import Popup from "../Portfolio/Popup";
import { PopupContainer } from "../Portfolio/PortfolioElements";
import "../style.css";

const Watchlist = () => {
  const { user } = React.useContext(UserContext) as UserContextType;

  const [stocks, setStocks] = useState<StockWithValue[]>([]);
  const [value, setValues] = useState<Value[]>([]);
  const [refetch, setRefetch] = useState(0);
  const [buttonPopup, setButtonPopup] = useState(false);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/watchlist/${user.id}`, {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    })
      .then((response) => response.json())
      .then((data) => setStocks(data));

    setValues([]);
    fetch(`${process.env.REACT_APP_BACKEND_URL}/purchases/stocks`, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((data) => setValues(data));
  }, [refetch]);

  function getGrowthRateColor(growthrate: string): string {
    if (growthrate.at(0) == "-") {
      return "red";
    }
    if (growthrate.at(0) == "+") {
      return "green";
    }
    return "grey";
  }

  function getGrowthRate(growthrate: string): string {
    if (growthrate.at(0) == "-") {
      return growthrate + " " + "%       " + "\u2b07";
    }
    if (growthrate.at(0) == "+") {
      return growthrate + " " + "%       " + "\u2B06";
    }
    return growthrate + "%";
  }

  function getAnlagetype(isin: string): string {
    let type: string = "";
    stocks.map((s) => {
      if (s.isin === isin) {
        type = s.type;
      }
    });
    if (!type) {
      return "";
    } else {
      return type;
    }
  }

  function getKurs(isin: string): string {
    let kurs;
    stocks.map((s) => {
      if (s.isin === isin) {
        kurs = s.stockprice;
      }
    });
    if (!kurs) {
      return "";
    } else {
      return kurs;
    }
  }

  function getTrend(isin: string): string {
    let trend: string = "";
    stocks.map((s) => {
      if (s.isin === isin) {
        trend = s.growthrate;
      }
    });
    if (!trend) {
      return "";
    } else {
      if (trend.at(0) == "-") {
        return trend + " " + "%       " + "\u2b07";
      }

      if (trend.at(0) == "+") {
        return trend + " " + "%       " + "\u2B06";
      }
      return trend + "%";
    }
  }

  function getLineGraph(isin: String) {
    const arr: Value[] = [];
    let dateString: string;

    value.map((i) => {
      if (i.sv_isin === isin) {
        dateString = i.currentdate.substring(0, 10);
        i.currentdate = dateString;
        arr.push(i);
      }
    });

    const dataSet = {
      labels: arr.map((item) => item.currentdate),
      datasets: [
        {
          data: arr.map((item) => item.stockprice),
          borderColor: "rgb(255, 99, 132)",
          backgroundColor: "rgba(255, 99, 132, 0.5)",
        },
      ],
    };

    const option = {
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: false,
        },
        scales: {
          scales: {
            x: [
              {
                ticks: {
                  display: false,
                },
              },
            ],
            yAxes: [
              {
                display: false,
              },
            ],
          },
        },
      },
    };

    return <Line options={option} data={dataSet} />; //<Line data={}/>
  }

  function deleteItemWatchlist(userid: number, isin: string) {
    const item = {
      userid: userid,
      isin: isin,
    };

    fetch(
      `${process.env.REACT_APP_BACKEND_URL}/watchlist/deleteWatchlistItem`,
      {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(item),
      }
    )
      .then((response) => response.text())
      .then((text) => {
        if (text.toString() === "OK") {
          setRefetch(Math.random());
        } else {
          toast(
            `❌ Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut! `
          );
        }
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  }

  if (!stocks.length) {
    return (
      <>
        <div className="wrapper">
          <div className="header">
            <h2>Watchlist</h2>
            <p style={{ textAlign: "center", marginTop: "10rem" }}>
              Noch keine Aktien vorgemerkt!
            </p>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="wrapper">
        <div className="header">
          <h2>Watchlist</h2>
        </div>
        <div className="body">
          <div className="main-container">
            <div className="table-container">
              <div className="table-row headinger">
                <div className="row-itemsmall">
                  <AiFillStar size={25} />
                </div>
                <div className="row-item2">Name</div>
                <div className="row-item">ISIN</div>
                <div className="row-item">Anzahl</div>
                <div className="row-item">Kurs</div>
                <div className="row-item3">Graph</div>
                <div className="row-item">Trend</div>
              </div>
              <div>
                {stocks.map((stock) => (
                  <div className="table-row" key={stock.isin}>
                    <div
                      className="row-itemsmall"
                      onClick={() => deleteItemWatchlist(user.id, stock.isin)}
                    >
                      <AiFillStar size={25} />
                    </div>
                    <div className="row-item2">{stock.name}</div>
                    <div className="row-item">{stock.isin}</div>
                    <div className="row-item">{stock.type}</div>
                    <div className="row-item">{stock.stockprice}€ </div>
                    <div
                      className="row-item3"
                      onClick={() => setButtonPopup(true)}
                    > {/* */}
                      {getLineGraph(stock.isin)}
                    </div>
                    <div className="row-item">
                      {" "}
                      <div
                        style={{ color: getGrowthRateColor(stock.growthrate) }}
                      >
                        {" "}
                        {getGrowthRate(stock.growthrate)}
                      </div>{" "}
                    </div>

                    <Popup trigger={buttonPopup} setTrigger={setButtonPopup}>
                      <h3>
                        {stock.name} - ISIN: {stock.sv_isin}
                      </h3>
                      <PopupContainer>
                        <p>Anlagetyp: {getAnlagetype(stock.sv_isin)}</p>
                        <p> Wert: {getKurs(stock.sv_isin)}€</p>
                        <p>
                          Trend:{" "}
                          <span
                            style={{
                              color: getGrowthRateColor(
                                getTrend(stock.sv_isin)
                              ),
                            }}
                          >
                            {getTrend(stock.sv_isin)}
                          </span>
                        </p>
                      </PopupContainer>
                      <div>{getLineGraph(stock.sv_isin)} </div>
                    </Popup>

                    <ToastContainer
                      position="top-center"
                      autoClose={3000}
                      hideProgressBar={false}
                      newestOnTop={false}
                      closeOnClick
                      rtl={false}
                      pauseOnFocusLoss
                      draggable
                      pauseOnHover
                      theme="light"
                      style={{ width: "80%", zIndex: 1000, top: 100 }}
                    />
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Watchlist;
