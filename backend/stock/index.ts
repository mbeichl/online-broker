import { Router, /*Request, Response*/ } from "express";
import stockController from "./controller";

const stockRouter = Router();

stockRouter.get('/getAllStocks', stockController.getAllStocksWithValue);
// stockRouter.post('/insertAllstocks', stockController.insertAllStocks);
stockRouter.post('/insertStockValues', stockController.insertStockValues_10);
stockRouter.post('/insertStockValues_10_PastMonth', stockController.insertStockValues_10_PastMonth);
stockRouter.post('/buy', stockController.buy);
stockRouter.post('/getGrowthratesJSON',stockController.getGrowthratesJSON)

export default stockRouter;
