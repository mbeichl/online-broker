import { Request, Response } from "express";
import accModel from "./model";
import { User } from "./User";


const accController = {
   async getUser(request: Request, response: Response): Promise<void> {
       const user = await accModel.getUser(request.params.username);
       response.json(user);
  },
  changePassword(request: Request, response: Response): void {
    //accModel.changePassword();
  },
  deleteAccount(request: Request, response: Response): void {
    const uid = request.body.userid;
    accModel.deleteAccount(uid);
  },
  async getCheatcards(request: Request, response: Response): Promise<void> {
    var userid = request.body.userid;

    var user : User = await  accModel.getUserByID(userid);
   
  }
};

export default accController;
