import {useState, useEffect} from 'react'
import { IconContext } from 'react-icons/lib'
import { animateScroll as scroll } from 'react-scroll'
import { Nav, NavbarContainer, NavLogo, NavBtn, NavBtnLink, NavBtnWrapper } from './NavbarElements'


const Navbar = () => {
    const [scrollNav, setScrollNav] = useState(false)

    const changeNav = () => {
        if(window.scrollY >= 80){
            setScrollNav(true)
        }else{
            setScrollNav(false)
        }
    }

    useEffect(() => {
      window.addEventListener('scroll', changeNav)},[])
    


      const toogleHome = () => {
        scroll.scrollToTop()
      }


  return (
    <>
    <IconContext.Provider value={{ color: '#fff'}}>
        <Nav> 
            <NavbarContainer>
                <NavLogo to='/' onClick={toogleHome}>ONLINE-BROKER</NavLogo>
                <NavBtnWrapper>
                    <NavBtn>
                        <NavBtnLink to='/signin'>Login</NavBtnLink>
                    </NavBtn>
                    <NavBtn>
                        <NavBtnLink to='/register'>Registrieren</NavBtnLink>
                    </NavBtn>
                </NavBtnWrapper>
            </NavbarContainer>
        </Nav>
        </IconContext.Provider>
    </>
  )
}

export default Navbar