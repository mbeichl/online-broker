import styled from 'styled-components'



export const PopupContainer = styled.div`
    columns: 2 auto;

`


export const PortfolioContainer = styled.div`
    background: #f8f8f8;
    padding: 20px 40px 5px 30px;
    display: flow-root;
    /* justify-content: flex-start;
    align-items: flex-start; */
    /* position: relative; */
    margin-top: 80px;

    /* height: 800px; */

    z-index: 1; */

    :before {
        /* content: '';
        position: absolute;
        z-index: 2;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0; */
        /* background: linear-gradient(180deg, rgba(0,0,0,0.2) 0%, 
        rgba(0,0,0,0.6) 100%), linear-gradient(180deg, rgba(0,0,0,0.2) 0%, transparent 100%); */
       
    }
`


export const SummaryWrapper = styled.div`
    z-index: 2;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    //position: absolute;
    padding: 8px 24px;
    /* max-width: 1200px; */
    
`

export const SummaryH1 = styled.h1`
    color: #000;
    font-size: 42px;
    text-align: start;

    /* @media screen and (max-width: 768px){
        font-size: 38px;
    }

    @media screen and (max-width: 480px){
        font-size: 30px;
    } */
`

export const SummaryH2 = styled.h1`
    color: #000;
    font-size: 36px;
    text-align: start;

    /* @media screen and (max-width: 768px){
        font-size: 38px;
    }

    @media screen and (max-width: 480px){
        font-size: 30px;
    } */
`

export const DetailedWrapper = styled.div`
    z-index: 3;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    //position: absolute;
    padding: 8px 24px;
    /* max-width: 1200px; */
    
`


export const DetailedRowWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: end;
    
    height: 150px;
    width: 100%;
    //position: absolute;
    padding: 8px 24px;
    border-bottom: 2px solid #a0a0a0;
    /* max-width: 1200px; */
    background-color: blueviolet;
`

//############################################################################################################
export const DetailedRowWrapperHeader = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: end;
    
    height: 50px;
    width: 100%;
    padding: 8px 24px;
    border-bottom: 2px solid #a0a0a0;
    background-color: orange;
`

export const NameHeader = styled.div`
    background-color: gray;
    height: 100%;
    margin-right: auto;
    color: #000;
    font-size: 28px;
    text-align: start;
`
export const AnzahlHeader = styled.div`
    color: #000;
    font-size: 28px;
    text-align: end;
    background-color: red;
    height: 100%;
    margin-left: 125px;
`


export const ValueHeader = styled.div`
    color: #000;
    font-size: 28px;
    text-align: start;
    background-color: aquamarine;
    height: 100%;
    margin-left: 50px;
    width: 150px;
`
export const GraphHeader = styled.div`
    color: #000;
    font-size: 28px;
    text-align: start;
    background-color: green;
    height: 100%;
    margin-left: 30px;
    width: 300px;
`

export const TrendHeader = styled.div`
    color: #000;
    font-size: 28px;
    text-align: end;
    background-color: yellow;
    height: 100%;
    margin-left: 20px;
    width: 75px;
`

export const VerkaufenHeader = styled.div`
    background-color: yellow;
    height: 100%;
    margin-left: 20px;
    width: 75px;
    color: #000;
    font-size: 28px;
    text-align: start;
`
export const VerkaufenBtnHeader = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    /* background-color: yellow; */
    height: 100%;
    margin-left: 20px;
    width: 75px;
`

export const Name = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: gray;
    height: 100%;
    margin-right: auto;
`
    

export const Anzahl = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: red;
    height: 100%;
    margin-left: 125px;
`


export const Value = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: aquamarine;
    height: 100%;
    margin-left: 50px;
    width: 125px;
`
export const Graph = styled.button`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: green;
    height: 100%;
    margin-left: 30px;
    width: 300px;
    border: none;
`

export const Trend = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: yellow;
    height: 100%;
    margin-left: 20px;
    width: 75px;
`



export const Verkaufen = styled.div`
    /* color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px; */
    background-color: yellow;
    height: 100%;
    margin-left: 20px;
    width: 75px;
`
export const VerkaufenBtn = styled.button`

    background-color: #FF0000;
    height: 25%;
    margin-left: 20px;
    width: 75px;
    padding: 20;
    border-radius: 5px;

    :hover{
        background-color: #FF6666;
    }
    
`


//###################################################################################################################

export const DetailedRowHeader = styled.h1`
    color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px;
    /* background-color: red; */
`

export const DetailedRowWert = styled.h1`
    color: #000;
    font-size: 28px;
    text-align: start;
    margin-right: 25px;
    background: blue;
`


export const DetailedRowGraph = styled.div`
`


