
import { is } from 'date-fns/locale';
import { response } from 'express';
import { createConnection } from 'mysql2/promise';
import { StockWithValue } from '../stock/objects/StockWithValue';
import DateLib from '../hilfsmethoden/DateLib';
import { Value } from '../stock/objects/Value';
import accModel from '../account/model';

// create the connection to database
const connection = createConnection({
    host: 'localhost',
    user: 'root',
    password: 'testbroker',
    database: 'wtbroker',
});


type Purchase = {
    id: number;
    userid: number;
    s_isin: string;
    purchasedate: string;
    purchaseprice: number;
    quantity: number;
};
type Values = {
    date: string;
    value: number;
}

type PurchaseQuantity = {
    name: string;
    sv_isin: string;
    quantity: number;
}

type Transaction = {
    userid: number,
    s_isin: string,
    type: string,
    date: string,
    price: number,
    quantity: number;
}



const purchasesModel = {
    /* #region  USE CASES */

    async getPurchases(userid: number) {
        const [rows, fields] = await (
            await connection
        ).execute(
            'SELECT id, userid, s_isin, date, type, price, quantity FROM `purchases` WHERE userid = ?',
            [userid]
        );
        console.log(rows);
        return (rows as Purchase[]);
    },
    async buy(isin: string, type: string, price: number, quantity: number, userid: number): Promise<boolean> {
        debugger;
            var userAccountValue : number =  (await accModel.getUserByID(userid)).accountvalue;
            //check if user can afford the stocks
            if(userAccountValue - (price * quantity) > 0 ){
            if (! await this.stockAlreadyBought(isin, userid,type)) {
                //Purchase in purchases inserten
                await (await connection).query(
                    `INSERT INTO purchases (id, userid , s_isin, type, date,price, quantity) VALUES (null, ${userid},'${isin}','${type}','${DateLib.getYesterdaysDate()}',${price},${quantity});`
                )
                //console.log(`inserted purchases ${isin} ${price}, ${quantity}`)

            }
            else {
                //nur quanity updaten anstatt insert
                await (await connection).query(
                    `UPDATE purchases
                    SET quantity = quantity + ${quantity}
                    WHERE userid = ${userid} and s_isin = '${isin}';`
                )
            }

            //User Accountvalue € updaten
            await (await connection).query(
                `UPDATE users
                SET accountvalue = accountvalue - (${price} * ${quantity}), portfolio = portfolio +  (${price} * ${quantity})
                WHERE id = ${userid};`
            )
            //console.log(`updated purchases ${isin} ${price}, +${quantity} Stück`)

            return true;

        }
        else{
            return false;
        }
        
    },

    async buyPast(isin: string, type: string, price: number, quantity: number, userid: number, date : string ): Promise<boolean> {
        debugger;
        var acc = await accModel.getUserByID(userid);
        var userAccountValue : number =  (await accModel.getUserByID(userid)).accountvalue;
        var userCheatCards : number =  (await accModel.getUserByID(userid)).cheatcards;

        //check if user can afford the stocks
        if(userAccountValue - (price * quantity) > 0 && userCheatCards > 0){
        if (! await this.stockAlreadyBought(isin, userid,type)) {
            //Purchase in purchases inserten
            await (await connection).query(
                `INSERT INTO purchases (id, userid , s_isin, type, date,price, quantity) VALUES (null, ${userid},'${isin}','${type}','${date}',${price},${quantity});`
            )
            //console.log(`inserted purchases ${isin} ${price}, ${quantity}`)

        }
        else {
            //nur quanity updaten anstatt insert
            await (await connection).query(
                `UPDATE purchases
                SET quantity = quantity + ${quantity}
                WHERE userid = ${userid} and s_isin = '${isin}';`
            )
        }

        //User Accountvalue € updaten
        await (await connection).query(
            `UPDATE users
            SET accountvalue = accountvalue - (${price} * ${quantity}), cheatcards = cheatcards -1
            WHERE id = ${userid};`
        )
        //console.log(`updated purchases ${isin} ${price}, +${quantity} Stück`)

        return true;

    }
    else{
        return false;
    }
    
},

    async getPurchasedQuantity(userid: number) {

        const [rows, fields] = await (
            await connection
        ).execute(
            'SELECT stocks.isin AS sv_isin, stocks.name, SUM(purchases.quantity) AS quantity \
            FROM stocks INNER JOIN purchases ON stocks.isin = purchases.s_isin \
            WHERE stocks.isin IN ( \
                SELECT s_isin \
                FROM purchases  \
                WHERE userid = ?) \
            AND purchases.userid = ? \
            GROUP BY purchases.userid, stocks.isin, stocks.name',[userid, userid]
        );
        

        const resultDB = rows as PurchaseQuantity[];
        const result: PurchaseQuantity[] = [];
        resultDB.map((r) => {
            if(r.quantity != 0){
                result.push(r);
            }
        })
        return result

    },
    async getAllValuesToISIN() { //isin: string

        const [rows, fields] = await (
            await connection
        ).execute(
            'select sv_isin, currentdate, stockprice from stockvalues ORDER BY currentdate' // where sv_isin = ?',[isin]
        );

        return (rows as Value[]);

    },
    async sell(transaction: Transaction) {

        const [rows, fields] = await (
            await connection
        ).execute(
            'INSERT INTO purchases (userid, s_isin, type, date, price, quantity) VALUES (?, ?, ?, ?, ?, ?)',[transaction.userid, transaction.s_isin, transaction.type, transaction.date, transaction.price, transaction.quantity]
        );

        // return (rows as Value[]);

    },
    /* #endregion */

    /* #region  HILFSMETHODEN */

    async stockAlreadyBought(isin: string, userid: number, type : string): Promise<boolean> {

        type Purchase = {
            id: number;
            userid: number;
            s_isin: string;
            type: string;
            purchasedate: string;
            purchaseprice: number;
            quantity: number;
        };

        const conn = await connection;
 
        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * from purchases`);

        const purchases: Purchase[] = rows as Purchase[];
        
        for(let i = 0; i <purchases.length; i++){
            //typeverlgiech nur für Feature: Unterscheidung von Cheat und Nciht Cheat Käufen in Portfilio
            if(purchases[i].s_isin == isin && purchases[i].userid == userid/* && purchases[i].type == type */){
                return true
            }
        }
            return false;
        
    }


    /* #endregion */

};

export default purchasesModel;




