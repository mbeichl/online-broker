
export type UserDataComplete = {
    id: number;
    firstName: string;
    lastName: string;
    userName: string;
    email: string;
    password: string;
    accountvalue: number;
    portfolio: number;
  }