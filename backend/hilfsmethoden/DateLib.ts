import { format } from "date-fns";


//globale Hilfsmithoden für Dates


const DateLib = {
     /**
    * ANNAHME in der gesamten Applikation [HEUTE = (Date.now - 4) ] , Grund: API bietet bei einigen Akiten erst ein paar Tage spätr den Wert
    */
     getYesterdaysDate(): string {

        var yesterdaysDate = new Date();
        yesterdaysDate.setDate(yesterdaysDate.getDate() - 4);
        var yesterdaysDateString : string = format(yesterdaysDate, 'yyyy-MM-dd')

        //OHNE TAGESZEIT ZURÜCKGEBEN!!
        var splitArray : string[] = yesterdaysDateString.split("T");
        var yesterdaysDateWithoutTime : string = splitArray[0];
        return yesterdaysDateWithoutTime;
    },

      /**
    * ANNAHME in der gesamten Applikation [HEUTE = (Date.now - 3) ]
    */
        getDate_X_DaysAgo(days_ago : number ): string {

        var yesterdaysDate = new Date();
        yesterdaysDate.setDate(yesterdaysDate.getDate() - days_ago);
        var yesterdaysDateString : string = format(yesterdaysDate, 'yyyy-MM-dd')

        //OHNE TAGESZEIT ZURÜCKGEBEN!!
        var splitArray : string[] = yesterdaysDateString.split("T");
        var yesterdaysDateWithoutTime : string = splitArray[0];
        console.log("DATELIB." + yesterdaysDateWithoutTime);

        return yesterdaysDateWithoutTime;
    },
    cutDateToOnlyDay(date : string): string {

        var re : string = date.substring(0,10);
        
        return re;
    }
}

 export default DateLib;