import React, { useContext } from 'react'
import { UserContext, UserContextType } from '../../UserProvider';
import { AccountContainer, AccountContent, GreetingsWrapper, GreetingsH1, AccountP } from './AccountElements'
import './style.css'

const Account = () => {
    const { user } = useContext(UserContext) as UserContextType;


  return (
    <>
    <AccountContainer>
        <GreetingsWrapper>
            <GreetingsH1>Schön dich zu sehen! </GreetingsH1>
        </GreetingsWrapper>

        <AccountContent>
            <AccountP>verfügbarer Einsatz: {user.accountvalue} €</AccountP>
        </AccountContent>
        <div className="schweizer-franke">
        <img height="800" width="400"src="https://touchcoins.moneymuseum.com/coins_media/Schweizerische-Eidgenossenschaft-20-Franken-8-Banknotenserie-in-Kurs-seit-1995/387/reverse.png"></img> 

        </div>
    </AccountContainer>
    

    </>
    
  )
}

export default Account