import axios from 'axios';
import { Stock } from '../stock/objects/Stock';
import { Value } from '../stock/objects/Value';
import { format } from 'date-fns';
import { writeFile } from 'node:fs/promises';
import DateLib from '../hilfsmethoden/DateLib';
import api_keysJson from "./api_keys.json";


export const apiModel = {


  /**
    * @return  Kurs-Objekt des Kurses von Gestern der Aktie mit der isin, ausgelesen aus kurs.json mit hilfe von schreibeAPI_KursJSOON
    */
   async getKursGesternObjekt(isin: string) : Promise<Value>{
    var yesterdaysDate : string = DateLib.getYesterdaysDate();
    var reKurs : Value = {
      sv_isin: "-1",
      stockprice: -1,
      currentdate: yesterdaysDate
    }
    console.log(`getting StockPrice of ${isin}}`)

    await axios.get(`https://data.lemon.markets/v1/ohlc/d1?isin=${isin}&from=${yesterdaysDate}&limit=1`,
      {
        headers: {
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsZW1vbi5tYXJrZXRzIiwiaXNzIjoibGVtb24ubWFya2V0cyIsInN1YiI6InVzcl9xeVFoVEpKaGg4NUJiSmp5cVRQaHBMWTd4TjZWYjIzQ21CIiwiZXhwIjoxNzAzNjA2MjQ4LCJpYXQiOjE2NzIwNzAyNDgsImp0aSI6ImFwa19xeVFoVEpKa2tOZFI1cWgzR210YlJ0UVk5QnE2UXM0endnIiwibW9kZSI6InBhcGVyIn0.ftyOfBvFNabrNSSjAYunNJNwrV7PVa447v4kSUPcai8' ,
        }
      })
      .then((response) => {
         reKurs = {
          sv_isin: response.data.results[0].isin,
          stockprice: response.data.results[0].c,
          currentdate: yesterdaysDate
        }
      })
      .catch((e : string) => {
        
        var error : string = e;
         reKurs =  {
          sv_isin:  `API Exception in apiModel.getKursGesternObjekt(), EXCEPTION VERSCHLUCKT `,
          //${e.substring(0, 30)},
          stockprice: -1,
          currentdate: yesterdaysDate
        };

        return reKurs;
        
      });
      
      
      return reKurs;
  },

 /**
    * @return  Kurs-Objekt des Kurses von Gestern der Aktie mit der isin, ausgelesen aus kurs.json mit hilfe von schreibeAPI_KursJSOON
    */
 async getKurs_X_DaysAgo_Object(isin: string, days_ago :number ) : Promise<Value>{
  var yesterdaysDate : string = DateLib.getDate_X_DaysAgo(days_ago);
  var reKurs : Value = {
    sv_isin: "-1",
    stockprice: -1,
    currentdate: yesterdaysDate
  }
  console.log(`getting StockPrice of ${isin}}`)

  await axios.get(`https://data.lemon.markets/v1/ohlc/d1?isin=${isin}&from=${yesterdaysDate}&limit=1`,
    {
      headers: {
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsZW1vbi5tYXJrZXRzIiwiaXNzIjoibGVtb24ubWFya2V0cyIsInN1YiI6InVzcl9xeVFoVEpKaGg4NUJiSmp5cVRQaHBMWTd4TjZWYjIzQ21CIiwiZXhwIjoxNzAzNjA2MjQ4LCJpYXQiOjE2NzIwNzAyNDgsImp0aSI6ImFwa19xeVFoVEpKa2tOZFI1cWgzR210YlJ0UVk5QnE2UXM0endnIiwibW9kZSI6InBhcGVyIn0.ftyOfBvFNabrNSSjAYunNJNwrV7PVa447v4kSUPcai8' ,
      }
    })
    .then((response) => {
       reKurs = {
        sv_isin: response.data.results[0].isin,
        stockprice: response.data.results[0].c,
        currentdate: yesterdaysDate
      }
    })
    .catch((e : string) => {
      
      var error : string = e;
       reKurs =  {
        sv_isin:  `API Excpetion:${e}`,
        //${e.substring(0, 30)},
        stockprice: -1,
        currentdate: yesterdaysDate
      };
      return reKurs;
      
      
      //abfangen in controller


    });
    
    
    return reKurs;
},




  /**
    * @return  Kurs-Objekt des Kurses von Gestern der Aktie mit der isin, ausgelesen aus kurs.json mit hilfe von schreibeAPI_KursJSOON
    */
  async getKursGestern(isin: string) : Promise<number>{
    const fs = require('fs')

   var yesterdaysDate : string = DateLib.getYesterdaysDate();
    var reValue = -1;

  await axios.get(`https://data.lemon.markets/v1/ohlc/d1?isin=${isin}&from=${yesterdaysDate}&limit=1`,
     {
       headers: {
         'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsZW1vbi5tYXJrZXRzIiwiaXNzIjoibGVtb24ubWFya2V0cyIsInN1YiI6InVzcl9xeVFoVEpKaGg4NUJiSmp5cVRQaHBMWTd4TjZWYjIzQ21CIiwiZXhwIjoxNzAzNjA2MjQ4LCJpYXQiOjE2NzIwNzAyNDgsImp0aSI6ImFwa19xeVFoVEpKa2tOZFI1cWgzR210YlJ0UVk5QnE2UXM0endnIiwibW9kZSI6InBhcGVyIn0.ftyOfBvFNabrNSSjAYunNJNwrV7PVa447v4kSUPcai8' ,
       }
     })
     .then((response) => {

         debugger;
         //wird tatsächlch überschrieben =>vermutlich Problem mit Threading
         //Er gibt bereits während er hier am ausführen ist den Rückgabewert zurück! ???? Also gibt er den Rückgabewertz zurück bevor er überschrieben wird
         //=> await benutzen: https://stackoverflow.com/questions/71974111/function-returning-before-data-is-processes
         reValue = response.data.results[0].c; 
     })
     .catch((e) => {
       console.log(e);
       throw new Error(`API Exception ${e}`  );
     });
      
     return reValue;
 },

  /**
  *  zieht sich die Aktien aus der API als json und speichert diese ab als .json
  */
  schreibeAPI_AktienInJSON() {
    console.log("schreibeAktieninJSON");
    axios.get(`https://data.lemon.markets/v1/instruments`,
      {
        headers: { 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsZW1vbi5tYXJrZXRzIiwiaXNzIjoibGVtb24ubWFya2V0cyIsInN1YiI6InVzcl9xeVBiTWRkSEg5RkNzWlB5eE5RVDJDcDVoTmZYbEpaTHI4IiwiZXhwIjoxNzAwMjk5Njg4LCJpYXQiOjE2NjkwMjI4ODgsImp0aSI6ImFwa19xeVBiTWtrTExLUXdSU1pteXdIczFaRDlyd2s2MHdiMjZiIiwibW9kZSI6Im1hcmtldF9kYXRhIn0.DkLgyDZxraQLgD91xMa43MeS8GmfsTIo7w0lb0nkXqk' }
      })
      .then((response) => {
        //erstelle neues aktien.json file falls noch nicht vorhanden
        //schreibe gesamten json response in aktien.json

        var aktien_string: string = JSON.stringify(response.data);
        //console.log(aktien_string);
        writeFile('./api/aktien.json', aktien_string);

      })
      .catch((e) => {

        console.log(e);
        throw new Error('API Exception ');
      });
  },


  /**
   *  zieht sich den Kurs der Aktie mit übergebenem ISBN aus der API als json und speichert diese ab in der kurse.json und erstellt ggf diese json datei falls nicht vorhanden
   */
  async schreibeAPI_KursJSON(ISIN: string) {

    //Datum von gestern rausfinden
    var yesterdaysDate = new Date();
    yesterdaysDate.setDate(yesterdaysDate.getDate() - 3);
    var yesterdaysDateString = format(yesterdaysDate, 'yyyy-MM-dd')

    var token: string = `Bearer ${api_keysJson['Market Data Key']}`;
    console.log(`Token: ${token}`)
    var request: string = `https://data.lemon.markets/v1/ohlc/d1?isin=${ISIN}&from=${yesterdaysDateString}&limit=1`;

    console.log(request);



    axios.get(request,
      {
        headers: {
          "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsZW1vbi5tYXJrZXRzIiwiaXNzIjoibGVtb24ubWFya2V0cyIsInN1YiI6InVzcl9xeVFoVEpKaGg4NUJiSmp5cVRQaHBMWTd4TjZWYjIzQ21CIiwiZXhwIjoxNzAzNjA2MjQ4LCJpYXQiOjE2NzIwNzAyNDgsImp0aSI6ImFwa19xeVFoVEpKa2tOZFI1cWgzR210YlJ0UVk5QnE2UXM0endnIiwibW9kZSI6InBhcGVyIn0.ftyOfBvFNabrNSSjAYunNJNwrV7PVa447v4kSUPcai8",
        }
      })
      .then((response) => {
        console.log(' RESPONSE');
        var kurse_string: string = JSON.stringify(response.data);
        writeFile('./api/kurse.json', `{ ${kurse_string} } `);

      })
      .catch((e) => {
        console.log("CATCH");
        console.log(e);
        throw new Error('API Exception ');
      });
  }
}
