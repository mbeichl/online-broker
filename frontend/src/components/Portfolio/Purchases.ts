export type Purchases = {
    id: number;
    userid: number;
    s_isin: string;
    purchasedate: string;
    purchaseprice: number;
    quantity: number;
  };
  