# Setup

- `npm init -y` => package.json
- `npm add -D typescript` => installiert typescript
- `npm add -D ts-node-dev` => Node + Typescript
- `npx tsc --init` => Typescript config erzeugen
- `.gitignore` => node_modules nicht committen
- `npm add express`=> Web Application Framework
- `npm add -D @types/express` => typdefinition express

# Database

 **Database Info**:
- Name: wt-broker
- Password: testbroker
- Port: 33 06:3306

- `npm run start:db` => fährt den DB Container hoch
- `docker ps => zeigt die laufenden Container an
- `docker exec -it wt-broker /bin/sh` => verbindet sich mit der Shell des Containers
- `mysql -u root -p` => mysql client starten 
        passwort: testbroker
- DB Statements ausführen

# Get it up and running
d
**Voraussetzungen**: **Node** und **Docker** müssen installiert sein
**Voraussetzung der Voraussetzung (Windows)**: WSL muss installiert und aktiviert sein

- `wsl --install`
- `wsl --install -d Ubuntu` 
- https://learn.microsoft.com/de-de/windows/wsl/install-manual
- https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi

- `cd backend`
- `npm install`
- `npm run start:db`
- `docker exec -it wt-broker /bin/sh`
- `mysql -u root -p` => Passwort: testbroker
- Inhalt aus der `init-db.sql` kopieren und einfügen - dann
- `npm start`

x
