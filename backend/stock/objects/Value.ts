export  type Value = {
    sv_isin: string;

      /**
    * Value -1 steht für Error
    */
    stockprice: number;
    currentdate: string;
  };
