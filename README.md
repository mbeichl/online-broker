# Online Broker App

Die App ist eine Börsenapp mit der es möglich ist verschiedne Aktien zu kaufen und zu verkaufen.
Um das Projekt zu starten muss Node.js installiert sein und im npm CLI der Befehl "npm install" ausgeführt werden.

# Anmerkungen

Das Projekt war Teil eines Moduls in der TH Rosenheim
<BR>
<BR>
Teilnehmer: Michael Beichl, Elizabeth Haller, Markus Baudenbacher

## limitierte kostenlose API Version 
    - Da die kostenlose Version der Lemon Markets API nur 10 Zugriffe pro minute erlaubt, dauert es 30 Minuten nach dem Starten der App bis alle Akitenwerte des letzten Monats vorhanden sind.
    - Allerdings werden die Aktienwerte der letzten Wochen zu Fertigstellung des Projekts automatisch in die DB eingefügts.
