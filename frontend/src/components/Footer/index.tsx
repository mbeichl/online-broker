
import { FooterA, FooterP, FooterContainer, FooterLinksWrapper, FooterLinkItems } from './FooterElements'

const Footer = () => {
  return (
      <FooterContainer>
        <FooterLinksWrapper>
            <FooterP>WT-Projekt WS22/23</FooterP>
            <FooterLinkItems>
                <FooterA href="https://inf-git.fh-rosenheim.de/studbeicmi6188/online-broker" target="_blank">Git</FooterA>
                <FooterA href="https://www.lemon.markets/" target="_blank">Lemon Markets API</FooterA>
                <FooterA href="https://www.th-rosenheim.de/" target="_blank">TH Rosenheim</FooterA>
            </FooterLinkItems>
        </FooterLinksWrapper>
    </FooterContainer>
  )
}

export default Footer