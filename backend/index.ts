import express, { request, response } from 'express';
import authRouter from './auth';
import stockRouter from './stock';
import accRouter from './account';
import watchlistRouter from './watchlist';
import { port } from './constants';
import cors from 'cors';
import axios from 'axios';
import {apiModel} from './api/apiModel'
import { readFile, writeFile } from 'node:fs/promises';
import { Stock } from './stock/objects/Stock';
import { Value } from './stock/objects/Value';
import { Express } from 'express';
import stockController from './stock/controller';
import accController from './account/controller';
// import { StockValue } from './StockValue/StockValue';
import stockModel from './stock/model'
import purchasesRouter from './purchases';
import DateLib from './hilfsmethoden/DateLib';
import purchasesModel from './purchases/model';

//#REGION DEBUG API

//stockModel.getGrowthRate("AT00000FACC2");
//stockController.getAllStocksWithValue(request,response);
//stockModel.insertStockValues_X_Days_ago(20);
// for(let i = 0;  i < 10;i++){
//       setTimeout(() =>  console.log("fooo"),6000);
// }
// var i = 4;
// function foo(){
//     if(i < 30){
//     i++;
// stockModel.insertStockValues_X_Days_ago(i);
//     }
// }
// function callFoo(){
//     foo();
// }

// //10 api calls per miunute
// setInterval(callFoo, 61000);


// function delayedCall(i : number) : Promise<void> {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//         stockModel.insertStockValues_X_Days_ago(i)
//       resolve();
//     }, 1000);
//   });
// }

// async function main() {
//   for (let i = 4; i < 15; i++) {
//     await delayedCall(i);
//   }
// }

// main();


 

//purchasesModel.buy()
//stockModel.insertStockValues_10();
//stockController.insertAllStocks();
//stockController.insertStockValues();
//stockController.getAllStocksWithValue(request,response);
//stockModel.getGrowthRate("AT00000FACC2");

const app = express();
app.use(express.json());
app.use(cors());

app.get("stockController");
app.post("stockController");
app.get("purchasesController");
app.post("purchasesController");
app.get("authController");
app.post("authController");

app.get('/', (req, res) => res.send('Hello World!'));

app.use('/auth', authRouter);
app.use('/stocks', stockRouter);
app.use('/user', accRouter);
app.use('/purchases', purchasesRouter);
app.use('/watchlist', watchlistRouter);

//INIT BACKEND 

//Aktien initial wenn seite geladen wird bulk-inserten //NICHT ÄNDERN APPLIAKTIONSCODDE KEIN TEST


// app.use('/');
// app.use('/insertallstock', stockRouter);



app.listen(port, () => console.log(`listening to http://localhost:${port}`));
//  }
//  catch(e) {
    

//  }
