import styled from "styled-components";


export const AccountContainer = styled.div`
    background: #f8f8f8;
    padding: 20px 40px 5px 30px;
    display: flow-root;
    /* justify-content: flex-start;
    align-items: flex-start; */
    position: relative;
    margin-top: 80px;

    /* height: 800px; */

    z-index: 1; 
/*
    :before {
         content: '';
        position: absolute;
        z-index: 2;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0; */
        /* background: linear-gradient(180deg, rgba(0,0,0,0.2) 0%, 
        rgba(0,0,0,0.6) 100%), linear-gradient(180deg, rgba(0,0,0,0.2) 0%, transparent 100%); 
       
    }*/
`


export const GreetingsWrapper = styled.div`
z-index: 2;
display: flex;
flex-direction: column;
align-items: flex-start;
//position: absolute;
padding: 8px 24px;
/* max-width: 1200px; */
`



export const GreetingsH1 = styled.h1`
    color: #000;
    font-size: 42px;
    text-align: start;

    /* @media screen and (max-width: 768px){
        font-size: 38px;
    }

    @media screen and (max-width: 480px){
        font-size: 30px;
    } */
`


export const AccountContent = styled.div`
    /* z-index: 3; */
    /* max-width: 1200px; */
    position: absolute;
    padding: 8px 24px;
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const AccountP = styled.p`
    /* margin-top: 24px; */
    color: #000;
    font-size: 32px;
    text-align: start;
    /* max-width: 600px; */

    /* @media screen and (max-width: 768px){
        font-size: 24px;
    }

    @media screen and (max-width: 480px){
        font-size: 18px;
    } */
`