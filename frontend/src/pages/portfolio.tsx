import PortfolioPage from '../components/Portfolio'
import NavbarNachLogin from '../components/NavbarNachLogin'

const Portfolio = () => {

  return (
    <>
    {/* <Sidebar isOpen={isOpen} toggle={toggle} />*/}
    <NavbarNachLogin />  {/*toggle={toggle}*/}
    <PortfolioPage />
    </>
  )
}

export default Portfolio;