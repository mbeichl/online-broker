import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { secret } from '../constants';

export default function checkIsLoggedIn(
  request: Request,
  response: Response,
  next: NextFunction
): void {
  const token = request.headers.authorization?.split(' ')[1];
  let isValid = false;
  try {
    if (typeof token === 'string') {
      let decoded = jwt.verify(token, secret);
      isValid = !!decoded;
    }
  } catch (error) {
    console.error(error);
  }
  if (isValid) {
    next();
  } else {
    response.statusCode = 401;
    response.send('Unauthorized');
  }
}
