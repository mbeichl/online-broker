import React from "react";
import {
  FormInputWrapper,
  FormColumnContainer,
  Container,
  FormContent,
  FormWrap,
  Form,
  FormH1,
  FormLabel,
  FormInput,
  FormButton,
  Nav,
  NavbarContainer,
  NavLogo,
} from "./RegisterElements";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { RegisterForm } from "./RegisterForm";
import { ToastContainer, toast } from "react-toastify";

const Register: React.FC = () => {
  const { register, handleSubmit } = useForm<RegisterForm>();
  const navigate = useNavigate();

  function onRegis(values: RegisterForm) {
    const pw: string = (document.getElementById("pw") as HTMLInputElement)
      .value;
    const pwcheck: string = (
      document.getElementById("pwcheck") as HTMLInputElement
    ).value;

    if (pw == pwcheck) {
      fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/register`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(values),
      })
        .then((response) => response.text())
        .catch((error) => console.log(error))
        .then(() => navigate("/"));
    } else {
      toast(`❌ Passwörter stimmen nicht überein! `);
    }
  }

  return (
    <>
      <Container>
        {/* <div style={{ background: 'linear-gradient(108deg,rgba(1,147,86,1) 0%,rgba(10, 201, 122, 1) 100%' }}> */}
        {/* <div style={{marginTop: '80px'}}> */}
        <Nav>
          <NavbarContainer>
            <NavLogo to="/">ONLINE-BROKER</NavLogo>
          </NavbarContainer>
        </Nav>
        {/* </div> */}

        <FormWrap>
          <FormContent>
            <Form onSubmit={handleSubmit(onRegis)}>
              <FormH1>Registriere deinen neuen Account</FormH1>
              <FormColumnContainer>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Vorname</FormLabel>
                  <FormInput
                    type="text"
                    {...register("firstname")}
                    data-testid="firstname"
                    required
                  />
                </FormInputWrapper>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Nachname</FormLabel>
                  <FormInput
                    type="text"
                    {...register("lastname")}
                    data-testid="lastname"
                    required
                  />
                </FormInputWrapper>
              </FormColumnContainer>
              <FormColumnContainer>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Benutzername</FormLabel>
                  <FormInput
                    type="text"
                    {...register("username")}
                    data-testid="username"
                    required
                  />
                </FormInputWrapper>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Email</FormLabel>
                  <FormInput
                    type="text"
                    {...register("email")}
                    data-testid="email"
                    required
                  />
                </FormInputWrapper>
              </FormColumnContainer>
              <FormColumnContainer>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Passwort</FormLabel>
                  <FormInput
                    type="password"
                    {...register("password")}
                    data-testid="password"
                    id="pw"
                    required
                  />
                </FormInputWrapper>
                <FormInputWrapper>
                  <FormLabel htmlFor="for">Passwort wiederholen</FormLabel>
                  <FormInput
                    type="password"
                    data-testid="passwordwdh"
                    id="pwcheck"
                    required
                  />
                </FormInputWrapper>
              </FormColumnContainer>
              <FormColumnContainer>
                <FormButton type="submit" data-testid="submit-btn">
                  Registrieren
                </FormButton>
              </FormColumnContainer>
            </Form>
          </FormContent>
        </FormWrap>
        <ToastContainer
          position="top-center"
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
          style={{ width: "80%", top: 50 }}
        />
      </Container>
      {/* </div> */}
    </>
  );
};

export default Register;
