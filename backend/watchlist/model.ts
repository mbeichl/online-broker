import { createConnection } from 'mysql2/promise';
import DateLib from '../hilfsmethoden/DateLib';
import { StockWithValue } from '../stock/objects/StockWithValue';
import { Value } from '../stock/objects/Value';


// create the connection to database
const connection = createConnection({
    host: 'localhost',
    user: 'root',
    password: 'testbroker',
    database: 'wtbroker',
});



const watchlistModel = {
    async getWatchlist(userid: number): Promise<StockWithValue[]> {
        const conn = await connection;
        const yesterdaysDate: String = DateLib.getYesterdaysDate();
        var my_query = ` SELECT s.isin, s.name, s.type, sv.sv_isin, sv.stockprice, sv.currentdate FROM stockvalues sv, stocks s, watchlist w where w.isin = s.isin and sv.sv_isin = s.isin AND sv.currentdate='${yesterdaysDate}' and w.userid='${userid}';`;
        const [rows, fields] = await (
            await connection
        ).execute(my_query);

        const stockWithValues: StockWithValue[] = rows as StockWithValue[];
        for (let i = 0; i < stockWithValues.length; i++) {
            stockWithValues[i].growthrate = await this.getGrowthRateString(stockWithValues[i].isin);
        }
        debugger;

        return stockWithValues;
    },

    async getGrowthRateString(isin: string): Promise<string> {
        var growthRateString: string;

        var growthRate: number = await this.getGrowthRate(isin);

        if (growthRate > 0) {
            growthRateString = "+" + growthRate.toString();
        }
        else {
            growthRateString = growthRate.toString();
        }
        return growthRateString;
    },
    async getGrowthRate(isin: string): Promise<number> {

        const conn = await connection;
        const [rows, fields] = await (
            await connection
        ).execute(`select * from stockvalues where sv_isin='${isin}' order by currentdate desc;`);

        const stockValues: Value[] = rows as Value[];
        var growthRate: number;
        var growthRateRounded: number;

        if (await this.OtherValueExist(isin)) {
            var division: number = (stockValues[0].stockprice / stockValues[1].stockprice) * 100
            growthRate = division - 100;

            growthRateRounded = Math.round((growthRate + Number.EPSILON) * 100) / 100
        }
        else {
            growthRateRounded = 0.00;
        }
        console.log("Growthrate: " + growthRateRounded);
        return growthRateRounded;
    },
    async OtherValueExist(isin: string): Promise<boolean> {
        const conn = await connection;
        const [rows, fields] = await (
            await connection
        ).execute(`SELECT * FROM stockvalues where sv_isin='${isin}'`);

        const stockValues: Value[] = rows as Value[];
        if (stockValues.length > 1) {
            return true;
        }
        else {
            return false;
        }
    },


    async setWatchlist(userid: number, isin: string) {
        const conn = await connection;
        return conn.query(
            'INSERT INTO watchlist (userid, isin) VALUES (?,?)',[userid, isin]
        );

    },

    async deleteWatchlistItem(userid: number, isin: string) {

        const [rows, fields] = await (
            await connection
        ).execute(
            'DELETE FROM watchlist WHERE userid = ? AND isin = ?',[userid, isin]
        );
        return (rows );

    },

    async getWatchlistIsin(userid: number){
        const [rows, fields] = await (
            await connection
        ).execute(
            'SELECT isin FROM watchlist WHERE userid = ? ',[userid]
        );
        const result = rows as String[]

        return (result);

    }
};

export default watchlistModel;




