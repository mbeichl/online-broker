import { Request, Response } from "express";
import stockModel from "../stock/model";
import authModel from "./model";


//"Klasse"
const authController = {

  //"Methode"
  async login(request: Request, response: Response): Promise<void> {
    const username = request.body.username;
    const password = request.body.password;
    
    const token = await authModel.login({ username, password });

    if (token !== false) {
      response.send(token);
    } else {
      response.status(400).send("❌ Passwort oder Nutzername falsch");
    }
  },
  async register(request: Request, response: Response): Promise <void> {
    const userData = {
      firstName: request.body.firstname,
      lastName: request.body.lastname,
      userName: request.body.username,
      email: request.body.email,
      password: request.body.password,
      cheatcards: request.body.cheatcards,
    };
    const validate = await authModel.register(userData);
    //console.log("registering successful");
    if(validate !== false){ 
      stockModel.insertStockValues_Past_Month();  
      response.status(200).send('✅ Registrierung erfolgreich');
    }
    else{
      response.status(409).send('❌ Registrierung fehlgeschlagen');
    }
  },

  async getUser(request: Request, response: Response): Promise<void> {
    const username = request.params.username;
    const user = await authModel.getUserData(username);
    response.json(user);
  },

  async updateUser(request: Request, response: Response) : Promise<void> {
    const newData = {
      userid: parseInt(request.body.userid),
      accountvalue: parseFloat(request.body.accountvalue),
      portfoliovalue: parseFloat(request.body.portfoliovalue),
    }
    
    await authModel.updateUser(newData);
    response.send('true')
  },
};

export default authController;
