import React from "react";
import {
  Container,
  FormContent,
  FormWrap,
  Form,
  FormH1,
  FormLabel,
  FormInput,
  FormButton,
  Nav,
  NavbarContainer,
  NavLogo,
} from "./SigninElements";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Credentials } from "./Crendentials";
import { UserContext, UserContextType, IUser } from "../../UserProvider";

const SignIn = () => {
  const { saveUser } = React.useContext(UserContext) as UserContextType;
  const navigate = useNavigate();

  function onLogin(credentials: Credentials) {
    fetch(`${process.env.REACT_APP_BACKEND_URL}/auth/login`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(credentials),
    })
      .then((response) => response.text())
      .then((token) => {
        localStorage.setItem("token", token);
      })
      .catch((error) => console.log(error));

    if (localStorage.getItem("token") != null) {
      console.log("test");
      fetch(
        `${process.env.REACT_APP_BACKEND_URL}/auth/${credentials.username}`,
        {
          method: "POST",
          headers: { "Access-Control-Allow-Origin": "*" },
        }
      )
        .then((response) => response.json())
        .then((data) => saveUser(data[0] as IUser))
        .then(() => navigate("/overview"))
        .catch((error) => console.log(error));
    }
  }

  const { register, handleSubmit } = useForm<Credentials>();

  return (
    <>
      <div>
        <Nav>
          <NavbarContainer>
            <NavLogo to="/">ONLINE-BROKER</NavLogo>
          </NavbarContainer>
        </Nav>

        <Container>
          <FormWrap>
            <FormContent>
              <Form onSubmit={handleSubmit(onLogin)}>
                <FormH1>Log dich mit deinem Account ein</FormH1>
                <FormLabel htmlFor="for">Benutzername</FormLabel>
                <FormInput
                  type="text"
                  {...register("username")}
                  data-testid="username"
                  required
                />
                <FormLabel htmlFor="for">Passwort</FormLabel>
                <FormInput
                  type="password"
                  {...register("password")}
                  data-testid="password"
                  required
                />
                <FormButton type="submit" data-testid="submit-btn">
                  Einloggen
                </FormButton>
              </Form>
            </FormContent>
          </FormWrap>
        </Container>
      </div>
    </>
  );
};

export default SignIn;
