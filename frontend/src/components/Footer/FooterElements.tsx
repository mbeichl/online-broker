import styled from 'styled-components'


export const FooterContainer = styled.footer`
    background-color: #000;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
    padding-left: 15px;
`

export const FooterLinksWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
`

export const FooterLinkItems = styled.div`
    display: flex;
    flex-direction: row;
    text-align: left;
    box-sizing: border-box;
    color: #fff;
    align-items: center;
    justify-content: space-evenly;
    height: 100%;
    width: stretch;
`


export const FooterA = styled.a`
    color: #fff;
    text-decoration: none;
    font-size: 18px;

    &:hover {
        color: #01bf71;
        transition: 0.3s ease-out;
    }
`
export const FooterP = styled.p`
    color: #fff;
    width: 30%;
    padding-left: 50;
    font-size: 14px;
`