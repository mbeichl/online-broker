import { Router} from "express";
import purchasesController from "./controller";

const purchasesRouter = Router();

purchasesRouter.post('/sell', purchasesController.sell);
purchasesRouter.post('/:userid', purchasesController.getPurchases);
purchasesRouter.get('/purchases/:userid', purchasesController.getPurchasedQuantity);
purchasesRouter.get('/stocks', purchasesController.getAllValuesToISIN);
purchasesRouter.post('/buy', purchasesController.buy);

export default purchasesRouter;




