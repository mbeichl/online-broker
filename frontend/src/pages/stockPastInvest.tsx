import React from 'react'
import Footer from '../components/Footer'
import StocksPage from '../components/StocksPastInvest'
import NavbarNachLogin from '../components/NavbarNachLogin'


const Stocks: React.FC = () => {

    return(
        <>
        <NavbarNachLogin />
        <StocksPage />
        {/* <Footer /> */}
        </>
    )
}

export default Stocks;

