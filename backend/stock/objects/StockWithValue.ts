import stockController from "../controller";
import Stock from "../objects/Stock";
import { format } from "date-fns";
import stockModel from "../model";


  
    /**
       * @return nicht persistentes Transportobjekt in dem Eine Aktie mit sämtliche statischen und dynamischen Kursdaten steht
       */
export type StockWithValue = {
    isin: string;
    name: string;
    type: string;
    sv_isin: string;
    stockprice: number;
    currentdate: string;  
    growthrate: string;
  };


  
   
